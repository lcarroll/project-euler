#include <vector>
#include <unordered_set>
#include <algorithm>
#include <numeric>
#include <math.h>
#include <string>

namespace qst { 

void checkInputs(int, char**, std::string&, int&);

std::vector<bool> findPrimes(int);
int getNextPrime(int, const std::vector<bool>&);
bool isPrime(int);

bool isPalindrome(int);
bool isPalindrome(const std::string&);
bool isPandigital(long long, char, char);
bool isPandigital(const std::vector<long long>&, char, char);

int d(int);
void reduce(int&, int&);
long long concat(const std::vector<int>&);

// Checks command-line arguments for a file name and/or a number
void checkInputs(int nArgs, char** args, std::string &file, int &n) {
    if (nArgs == 1) return;

    if ((nArgs & 1) == 0 || nArgs > 5) {
        std::cerr << "Usage: " << args[0] << " [-f FILE] [-n NUMBER]\n";
        std::cerr << "Proceeding with defaults\n";
        return;
    }

    for (int i = 1; i < nArgs; i++) {
        if (std::string(args[i]) == "-f")
            file = args[++i];
        else if (std::string(args[i]) == "-n")
            n = std::stoi(args[++i]);
        else std::cerr << "Option " << args[i] << " not supported\n";
    }
}

// Uses a sieve to determine which numbers under some maximum are prime
std::vector<bool> findPrimes(int max) {
    std::vector<bool> isPrime((max >> 1) - 1, true);
    for (int i = 3; i < max; i = getNextPrime(i, isPrime)) {
        for (int j = ((i - 3) >> 1) + i; j < isPrime.size(); j += i)
            isPrime[j] = false;
    }

    return isPrime;
}

// Returns the next prime given a number and a vector containing true values
//  for primes, or returns the next highest odd number beyond what is 
//  represented in the vector
int getNextPrime(int n, const std::vector<bool> &isPrime) {
    auto getPrime = [](int i) { return (i << 1) + 3; };

    int i = ((n - 3) >> 1) + 1;
    if (isPrime[i] && getPrime(i) > n) return getPrime(i);

    while (i < isPrime.size() && !isPrime[i])
        i++;
        
    return getPrime(i);
}

// Checks if an integer is prime
bool isPrime(int n) {
    if (n < 2 || n > 2 && (n & 1) == 0) return false;
    if (n > 3 && n % 3 == 0) return false;

    int rt = sqrt(n);
    for (int i = 1; i * 6 - 1 <= rt; i++) {
        if (n % (i * 6 - 1) == 0) return false;
        if (n % (i * 6 + 1) == 0) return false;
    }

    return true;
}

// Checks if an integer is a palindrome
bool isPalindrome(int n) {
    std::string s = std::to_string(n);
    return isPalindrome(s);
}

// Checks if a string is a palindrome
bool isPalindrome(const std::string &s) {
    if (std::equal(s.begin(), s.begin() + (s.length() >> 1), s.rbegin()))
        return true;
    return false;
}

// Checks if a number contains every digit from ld to hd
bool isPandigital(long long n, char ld = 1, char hd = 9) {
    return isPandigital(std::vector<long long> {n}, ld, hd);
}

// Checks if the numbers in a vector contain every digit from ld to hd
bool isPandigital(const std::vector<long long> &nums, char ld = 1, char hd = 9) {
    if (ld < 0 || hd < 0 || ld > 9 || hd > 9) 
        return false;

    int cmp = (1 << (hd - ld + 1)) - 1;
    for (int i = 0; i < ld; i++)
        cmp <<= 1;

    int res = 0;
    for (long long num : nums) {
        long long n = num;
        while (n > 0) {
            char dig = n % 10;

            res |= 1 << dig;
            n /= 10;
        }
    }

    if (res == cmp) return true;
    else return false;
}

// Calculates and returns the sum of the proper divisors of n
int d(int n) {
    int sum = 1;

    int rt = sqrt(n);
    for (int i = 3; i <= rt; i += (n & 1) ? 2 : 1)
        if (n % i == 0) sum += (i + n / i);

    if ((n & 1) == 0) sum += 2 + n / 2;
    if (rt * rt == n) sum -= rt;

    return sum;
}

// Reduces two integers by their GCD
void reduce(int &a, int &b) {
    int factor = std::gcd(a, b);

    a /= factor;
    b /= factor;
}

// Concatenates integers into a single value
long long concat(const std::vector<int> &nums) {
    if (nums.empty()) return 0;

    long long cn = nums[0];
    for (int i = 1; i < nums.size(); i++) {
        if (nums[i] < 0) return 0;

        if (nums[i] > 0) 
            cn *= pow(10, (int) log10(nums[i]) + 1);
        else cn *= 10;

        cn += nums[i];
    }

    return cn;
}

}