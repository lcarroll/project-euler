#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>

std::vector<int> getRow(std::string&);

int main(int argc, char** argv) {
    std::string fileName = "p067_triangle.txt";
    if (argc > 1) fileName = argv[1];

    std::ifstream in(fileName);
    std::string line;
    std::getline(in, line);

    std::vector<int> prevRow, row;
    prevRow = getRow(line);

    while (std::getline(in, line)) {
        row = getRow(line);

        for (size_t j = 1; j < prevRow.size(); j++)
            row[j] += (prevRow[j] > prevRow[j - 1]) ? prevRow[j] : prevRow[j - 1];

        prevRow = row;
    }

    in.close();

    int max = 0;
    for (int num : row)
        if (num > max) max = num;

    std::cout << max << std::endl;
}

std::vector<int> getRow(std::string& line) {
    std::istringstream iss(line);

    std::vector<int> row = {0};
    int num;
    while (iss >> num) 
        row.push_back(num);
    row.push_back(0);

    return row;
}