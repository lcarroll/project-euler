#include <iostream>

typedef unsigned long long ull;

bool isPalindrome(ull);
ull reverse(ull);

int main() {
    int max = 10000;

    int lNums = max - 1;
    for (int i = 1; i < max; i++) {
        ull n = i + reverse(i);

        int nIters = 1;
        while (!isPalindrome(n) && nIters < 50) {
            n += reverse(n);
            nIters++;
        }

        if (nIters < 50) lNums--;
    }

    std::cout << lNums << std::endl;
}

bool isPalindrome(ull n) {
    if (n == reverse(n)) return true;
    return false;
}

ull reverse(ull n) {
    ull revN = 0;

    while (n > 0) {
        revN *= 10;
        revN += n % 10;
        n /= 10;
    }

    return revN;
}