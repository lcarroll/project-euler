#include <iostream>
#include <vector>
#include <math.h>

typedef unsigned long long ull;

bool isPerm(ull, ull);

int main(int argc, char** argv) {
    int nPerms = 5;
    if (argc > 1) nPerms = std::stoi(argv[1]);

    std::vector<ull> cubicPerms;

    int n = 5;
    while (cubicPerms.size() != nPerms) {
        std::vector<ull> cubes;

        // Generate all cubes with the same number of digits
        ull pow10 = pow(10, floor(log10(pow(n, 3))) + 1);
        while (pow(n, 3) < pow10) {
            cubes.push_back(pow(n, 3));
            n++;
        }

        // Check each cube against higher cubes for permutations
        for (size_t i = 0; i < cubes.size() - (nPerms - 1); i++) {
            ull cube = cubes[i];

            cubicPerms.clear();
            cubicPerms.push_back(cube);

            for (size_t j = i + 1; j < cubes.size(); j++) {
                if (isPerm(cube, cubes[j])) cubicPerms.push_back(cubes[j]);
            } 

            if (cubicPerms.size() == nPerms) break;
        }
    }

    std::cout << cubicPerms[0] << " (";
    for (ull cube : cubicPerms)
        std::cout << cube << ", ";
    std::cout << "\b\b)\n";
}

bool isPerm(ull n, ull m) {
    if ((int) log10(n) != (int) log10(m)) return false;

    std::vector<int> nDigCounts(10, 0), mDigCounts(10, 0);
    while (n > 0) {
        nDigCounts[n % 10]++;
        n /= 10;

        mDigCounts[m % 10]++;
        m /= 10;
    }

    for (size_t i = 0; i < nDigCounts.size(); i++)
        if (nDigCounts[i] != mDigCounts[i]) return false;
        
    return true;
}