#include <iostream>
#include <math.h>

bool isPerm(unsigned int, unsigned int);

int main() {
    int nMults = 6;
    bool isFound = false;

    unsigned int n = 0, nDigs = 3;
    while (!isFound) {
        n = pow(10, nDigs - 1);

        unsigned int max = 1;
        while (floor(log10(max)) + 1 < nDigs)
            max = max * 10 + 6;

        for (; n <= max; n++) {
            int i = 2;
            for (; i <= nMults; i++)
                if (!isPerm(n, i * n)) break;

            if (i > nMults) {
                isFound = true;
                break;
            }
        }

        nDigs++;
    }

    for (int i = 1; i <= nMults; i++)
        std::cout << i << " * " << n << " = " << i * n << std::endl;
}

bool isPerm(unsigned int n, unsigned int m) {
    if ((int) log10(n) != (int) log10(m)) return false;

    int res1 = 0, res2 = 0;
    while (n > 0) {
        res1 |= 1 << n % 10;
        n /= 10;

        res2 |= 1 << m % 10;
        m /= 10;
    }

    if (res1 == res2) return true;
    return false;
}