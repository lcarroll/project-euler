#include <iostream>
#include <math.h>

void reduce(int&, int&);

int main(int argc, char** argv) {
    int max = 10000;
    if (argc > 1) max = std::stoi(argv[1]);

    int cnt = 4;
    for (int i = 14; i <= max; i++) {
        int a = sqrt(i);
        if (a * a == i) continue;

        int n = a, d = 1, intPt = 0;

        int period = 0;
        while (intPt != a << 1) {
            d = (i - n * n) / d;

            intPt = (a + n) / d;
            n = abs(n - intPt * d);

            period++;
        }

        if ((period & 1)) cnt++;
    }

    std::cout << cnt << std::endl;
}

void reduce(int& n, int& d) {
    if (n == 1) return;

    if (d % n == 0) {
        d /= n;
        n = 1;
        return;
    }

    int rt = sqrt(n);
    for (int i = 2; i <= rt; i++) {
        while (n % i == 0 && d % i == 0) {
            n /= i;
            d /= i;
        }
    }
}