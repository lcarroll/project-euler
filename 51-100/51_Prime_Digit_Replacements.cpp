#include <iostream>
#include <unordered_set>
#include <string>
#include <algorithm>
#include <math.h>

bool isValidMask(int, int);
std::string applyMask(int, int);

int main(int argc, char** argv) {
    int famSize = 8;
    if (argc > 1) famSize = std::stoi(argv[1]);

    std::unordered_set<int> primes = {2};
    auto isPrime = [&primes](int n) {
        if (primes.count(n)) return true;
        if ((n & 1) == 0 && n > 2) return false;

        int rt = sqrt(n);
        for (int i = 3; i <= rt; i += 2)
            if (n % i == 0) return false;

        primes.emplace(n);
        return true;
    };

    auto genMasks = [famSize](int n) {
        char nDigs = floor(log10(n)) + 1;
        int maxMask = pow(2, nDigs) - 1;

        std::string str = std::to_string(n);
        std::unordered_set<int> masks;

        int mask = 0, idx = 0;
        while (mask < maxMask) {
            if (str[idx] - '0' > 10 - famSize) {
                mask += pow(2, --nDigs);
                idx++;
                continue;
            }

            int max = mask + pow(2, --nDigs);
            for (; mask < max; mask++)
                if (isValidMask(mask, n)) masks.emplace(mask);

            idx++;
        }

        return masks;
    };

    // Digits can only be replaced with digits equal to or greater than digit with highest significance being replaced 
    // (Thus, skip replacing when most significant digit > 10 - famSize)
    // First digit cannot be replaced with 0 or 1
    // Last digit cannot be replaced with an even digit or 5
    // At most n - 1 digits of an n-digit number can be replaced
    // Can either replace digits by themselves or repeated digits (cannot replace unequal digits)
    std::unordered_set<int> family;

    int n = 9;
    while (family.size() < famSize) {
        n += 2;
        if (!isPrime(n)) continue;

        std::unordered_set<int> masks = genMasks(n);
        for (int mask : masks) {
            family.clear();
            family.emplace(n);
            
            for (char i = '1'; i <= '9'; i++) {
                std::string str = applyMask(mask, n);
                std::replace(str.begin(), str.end(), 'x', i);

                int num = std::stoi(str);
                if (num <= n) continue;

                if (isPrime(num))
                    family.emplace(num);
            }

            if (family.size() == famSize) break;
        }
    }

    std::cout << n << " (";
    for (int prime : family)
        std::cout << prime << ", ";
    std::cout << "\b\b)\n";
}

bool isValidMask(int mask, int n) {
    std::string str = "";

    char d = 0;
    while (n > 0) {
        if (!(mask & 1)) {
            d = (n % 10) + '0';
            str += d;
        }

        mask >>= 1;
        n /= 10;
    }

    if (str.find_first_not_of(d) < str.length()) return false;
    return true;
}

std::string applyMask(int mask, int n) {
    std::string str = "";

    while (n > 0) {
        if (mask & 1) str += (n % 10) + '0';
        else str += 'x';

        mask >>= 1;
        n /= 10;
    }

    std::reverse(str.begin(), str.end());
    return str;
}