#include <iostream>

typedef unsigned long long ull;
ull combination(int, int);
ull fact(int);

int main(int argc, char** argv) {
    int maxN = 100, targVal = 1000000;
    if (argc > 2) targVal = std::stoi(argv[2]);
    if (argc > 1) maxN = std::stoi(argv[1]);

    int cnt = 0;
    for (int i = 23; i <= maxN; i++) {
        for (int j = 1; j <= i; j++) {
            if (combination(i, j) > targVal) {
                cnt += i - (j << 1) + 1;
                break;
            }
        }
    }

    std::cout << cnt << std::endl;
}

ull combination(int n, int r) {
    int sm = ((n - r) < r) ? n - r : r;
    int lg = ((n - r) > r) ? n - r : r;

    ull res = 1;
    for (int i = n; i > lg; i--)
        res *= i;

    return res / fact(sm);
}

ull fact(int n) {
    ull fact = 1;
    for (int i = n; i > 1; i--)
        fact *= i;

    return fact;
}