#include <iostream>
#include <vector>
#include <math.h>

void mult(std::vector<char>&, int);
int calcDigSum(const std::vector<char>&);

int main() {
    int max = 100;
    int maxDigSum = 18;

    for (int i = 2; i < max; i++) {
        if (i == 10) continue;

        int n = i;
        std::vector<char> power;
        while (n > 0) {
            power.push_back(n % 10);
            n /= 10;
        }

        for (int j = 2; j < max; j++) {
            mult(power, i);

            int digSum = calcDigSum(power);
            if (digSum > maxDigSum) maxDigSum = digSum;
        }
    }

    std::cout << maxDigSum << std::endl;
}

void mult(std::vector<char>& multiplicand, int multiplier) {
    int carry = 0;
    for (size_t i = 0; i < multiplicand.size(); i++) {
        carry += multiplicand[i] * multiplier;
        multiplicand[i] = carry % 10;
        carry /= 10;
    }

    while (carry > 0) {
        multiplicand.push_back(carry % 10);
        carry /= 10;
    }
}

int calcDigSum(const std::vector<char>& numVec) {
    int sum = 0;
    for (char dig : numVec)
        sum += dig;

    return sum;
}