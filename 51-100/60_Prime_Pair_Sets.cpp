#include <iostream>
#include <vector>
#include <unordered_set>
#include <math.h>

typedef unsigned long long ull;

std::vector<int> findMinVec(std::vector<int>&, int);
bool isPrime(ull);
int vecSum(std::vector<int>&);

std::unordered_set<ull> primes;

int main(int argc, char** argv) {
    int setSize = 5;
    if (argc > 1) setSize = std::stoi(argv[1]);

    std::vector<int> primeVec;
    for (int i = 3; i < 10000; i += 2)
        if (isPrime(i)) primeVec.push_back(i);

    std::vector<int> primeSet = findMinVec(primeVec, setSize);

    for (int prime : primeSet)
        std::cout << prime << " + ";
    std::cout << "\b\b= " << vecSum(primeSet) << std::endl;
}

std::vector<int> findMinVec(std::vector<int>& vec, int sz) {
    if (vec.size() < sz) return {};
    if (vec.size() == 1) return vec;

    std::vector<int> minVec;
    for (size_t i = 0; i < vec.size(); i++) {
        int n = vec[i];
        int nDigs = floor(log10(n)) + 1;

        std::vector<int> nVec;
        for (size_t j = i + 1; j < vec.size(); j++) {
            int m = vec[j];
            int mDigs = floor(log10(m)) + 1;

            if (isPrime(n * pow(10, mDigs) + m) && isPrime(m * pow(10, nDigs) + n))
                nVec.push_back(m);
        }

        nVec = findMinVec(nVec, sz - 1);
        if (!nVec.empty() && (minVec.empty() || vecSum(nVec) + n < vecSum(minVec))) {
            minVec = nVec;
            minVec.push_back(n);
        }
    }

    return minVec;
}

bool isPrime(ull n) {
    if ((n & 1) == 0 && n > 2) return false;
    if (primes.count(n)) return true;

    ull rt = sqrt(n);
    for (ull i = 3; i <= rt; i += 2)
        if (n % i == 0) return false;

    primes.emplace(n);

    return true;
}

int vecSum(std::vector<int>& vec) {
    int sum = 0;
    for (int n : vec)
        sum += n;

    return sum;
}