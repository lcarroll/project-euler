#include <iostream>
#include <vector>

void mult(std::vector<char>&, int);
void add(std::vector<char>&, const std::vector<char>&);
int calcDigSum(const std::vector<char>&);

int main(int argc, char** argv) {
    int nCons = 100;
    if (argc > 1) nCons = std::stoi(argv[1]);

    // num = intPt * prevNum + twicePrevNum;
    int intPt = 1;
    std::vector<char> n = {2}, pn = {1};
    for (int i = 1; i < nCons; i++) {
        if (i % 3 < 2) intPt = 1;
        else intPt = (i / 3 + 1) << 1;

        std::vector<char> tmp = n;
        mult(n, intPt);
        add(n, pn);
        pn = tmp;
    }

    std::cout << calcDigSum(n) << std::endl;
}

void mult(std::vector<char>& multiplicand, int multiplier) {
    int carry = 0;
    for (size_t i = 0; i < multiplicand.size(); i++) {
        carry += multiplicand[i] * multiplier;
        multiplicand[i] = carry % 10;
        carry /= 10;
    }

    while (carry > 0) {
        multiplicand.push_back(carry % 10);
        carry /= 10;
    }
}

void add(std::vector<char>& a1, const std::vector<char>& a2) {
    int carry = 0;
    for(size_t i = 0; i < a1.size(); i++) {
        carry += (i < a2.size()) ? a1[i] + a2[i] : a1[i];
        a1[i] = carry % 10;
        carry /= 10;
    }

    if (carry) a1.push_back(carry);
}

int calcDigSum(const std::vector<char>& numVec) {
    int sum = 0;
    for (char dig : numVec)
        sum += dig;

    return sum;
}