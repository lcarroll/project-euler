#include <iostream>
#include <math.h>

int main() {
    auto isPrime = [](int n) -> bool {
        if ((n & 1) == 0 && n > 2) return false;

        int rt = sqrt(n);
        for (size_t i = 3; i <= rt; i += 2)
            if (n % i == 0) return false;

        return true;
    };

    int i = 2, n = 5, inc = 2;
    int nPrimes = 1;

    while (i / nPrimes < 10 || (i & 3) > 0) {
        if ((i++ & 3) == 0) inc += 2;

        if (isPrime(n)) nPrimes++;
        n += inc;
    }

    std::cout << inc + 1 << std::endl;
}