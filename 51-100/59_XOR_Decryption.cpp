#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <map>

int main(int argc, char** argv) {
    std::string file = "p059_cipher.txt";
    if (argc > 1) file = argv[1];

    std::ifstream in(file);

    int keyLen = 3;
    std::vector<std::map<int, int>> charCounts(keyLen, std::map<int, int>());

    int i = 0;
    std::string encChar = "";
    while (std::getline(in, encChar, ',')) {
        int dec = std::stoi(encChar);
        
        int pos = i++ % keyLen;
        if (charCounts[pos].count(dec)) charCounts[pos][dec]++;
        else charCounts[pos][dec] = 1;
    }

    std::string key = "";
    for (int i = 0; i < keyLen; i++) {
        int chr, maxFreq = 0;
        for (auto entry : charCounts[i]) {
            if (entry.second > maxFreq) {
                maxFreq = entry.second;
                chr = entry.first;
            }
        }

        key += chr ^ ' ';
    }

    i = 0;
    in.clear();
    in.seekg(i);

    int sum = 0;
    while(std::getline(in, encChar, ',')) {
        char asciiVal = (std::stoi(encChar) ^ key[i++ % keyLen]);
        sum += asciiVal;

        std::cout << asciiVal;
    }

    in.close();

    std::cout << std::endl << std::endl << sum << std::endl;
}