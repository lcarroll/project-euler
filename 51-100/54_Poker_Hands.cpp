#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <unordered_map>

std::unordered_map<char, int> cardVals = {{'2', 0}, {'3', 1}, {'4', 2}, {'5', 3}, {'6', 4}, 
                                          {'7', 5}, {'8', 6}, {'9', 7}, {'T', 8}, {'J', 9}, 
                                          {'Q', 10}, {'K', 11}, {'A', 12}};
std::unordered_map<char, int> suitVals = {{'C', 0}, {'D', 1}, {'H', 2}, {'S', 3}};

enum ranks {HIGH_CARD, ONE_PAIR, TWO_PAIRS, THREE_OF_A_KIND, STRAIGHT, 
            FLUSH, FULL_HOUSE, FOUR_OF_A_KIND, STRAIGHT_FLUSH, ROYAL_FLUSH};

void getHand(const std::string&, std::vector<int>&, std::vector<int>&);
int playHand(const std::vector<int>&, const std::vector<int>&, int&, int&);
bool isFlush(const std::vector<int>&);
bool isStraight(const std::vector<int>&, int&);

int main(int argc, char** argv) {
    std::string file = "p054_poker.txt";
    if (argc > 1) file = argv[1];

    std::ifstream in(file);

    int nWins = 0;
    std::string game = "";
    while (std::getline(in, game)) {
        std::string hand = game.substr(0, game.length() >> 1);
        std::vector<int> p1Cards(13, 0), suits(4, 0);
        getHand(hand, p1Cards, suits);

        int p1RankCard = 0, p1HighCard = 0;
        int p1Rank = playHand(p1Cards, suits, p1RankCard, p1HighCard);

        if (p1Rank == ROYAL_FLUSH) {
            nWins++;
            continue;
        }

        hand = game.substr((game.length() >> 1) + 1);
        std::vector<int> p2Cards(13, 0);
        suits = std::vector<int>(4, 0);
        getHand(hand, p2Cards, suits);

        int p2RankCard = 0, p2HighCard = 0;
        int p2Rank = playHand(p2Cards, suits, p2RankCard, p2HighCard);

        if (p1Rank > p2Rank) {
            nWins++;
            continue;
        } else if (p1Rank < p2Rank) continue;

        if (p1RankCard > p2RankCard) {
            nWins++;
            continue;
        } else if (p1RankCard < p2RankCard) continue;

        if (p1HighCard > p2HighCard) { 
            nWins++;
            continue;
        } else if (p1HighCard < p2HighCard) continue;

        // Both players have two pairs of same value
        for (int i = p1Cards.size() - 1; i >= 0; i--) {
            if (p1Cards[i] == 1) {
                nWins++;
                break;
            } else if (p2Cards[i] == 1) break;
        }
    }

    std::cout << "Player 1 wins " << nWins << " games\n";
}

void getHand(const std::string& hand, std::vector<int>& cards, std::vector<int>& suits) {
    std::istringstream iss(hand);
    
    if (cards.size() != 13 || suits.size() != 4) return;

    char card, suit;
    while (iss >> card) {
        cards[cardVals.at(card)]++;

        iss >> suit;
        suits[suitVals.at(suit)]++;
    }
}

int playHand(const std::vector<int>& cards, const std::vector<int>& suits, int& rCrd, int& hCrd) {
    bool isFl = isFlush(suits);
    bool isSt = isStraight(cards, rCrd);

    if (isFl && isSt) {
        if (rCrd == cardVals['T']) return ROYAL_FLUSH;
        else return STRAIGHT_FLUSH;
    } else if (isFl) {
        int i = cards.size() - 1;
        while (!cards[i])
            i--;
        rCrd = i;
        return FLUSH;
    } else if (isSt) return STRAIGHT;

    int nPairs = 0;
    bool isTrip = false, isQuad = false;
    for (int i = 0; i < cards.size(); i++) {
        if (cards[i] == 1 && nPairs < 2) {
            hCrd = i;
        } else if (cards[i] == 2) {
            if (nPairs) hCrd = rCrd;
            nPairs++;

            if (!isTrip) rCrd = i;
            else hCrd = i;
        } else if (cards[i] == 3) {
            isTrip = true;
            rCrd = i;
        } else if (cards[i] == 4) {
            isQuad = true;
            rCrd = i;
            break;
        }
    }

    if (isQuad) return FOUR_OF_A_KIND;
    else if (isTrip && nPairs == 1) return FULL_HOUSE;
    else if (isTrip) return THREE_OF_A_KIND;
    else if (nPairs == 2) return TWO_PAIRS;
    else if (nPairs == 1) return ONE_PAIR;

    rCrd = hCrd;
    return HIGH_CARD;
}

bool isFlush(const std::vector<int>& suits) {
    for (int i = 0; i < suits.size(); i++) {
        if (suits[i] == 5) return true;
        else if (suits[i] > 0) return false;
    }

    return false;
}

bool isStraight(const std::vector<int>& cards, int& card) {
    int i = cards.size() - 1;
    while (cards[i] < 1)
        i--;
    card = i;

    int nCards = 0;
    for (; i >= 0; i--) {
        if (cards[i] != 1) return false;

        nCards++;
        if (nCards == 5) break;
    }

    return true;
}