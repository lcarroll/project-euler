#include <iostream>
#include <vector>
#include <math.h>

void mult(std::vector<char>&, int);

int main() {
    int cnt = 0;
    for (int i = 1; i < 10; i++) {
        cnt += floor(log(10)/(log(10) - log(i)));
        
        // std::vector<char> nPower = {1};
        // mult(nPower, i);

        // int power = 1;
        // while (nPower.size() == power) {
        //     cnt++;

        //     mult(nPower, i);
        //     power++;
        // }
    }

    std::cout << cnt << std::endl;
}

void mult(std::vector<char>& multiplicand, int multiplier) {
    int carry = 0;
    for (size_t i = 0; i < multiplicand.size(); i++) {
        carry += multiplicand[i] * multiplier;
        multiplicand[i] = carry % 10;
        carry /= 10;
    }

    while (carry > 0) {
        multiplicand.push_back(carry % 10);
        carry /= 10;
    }
}