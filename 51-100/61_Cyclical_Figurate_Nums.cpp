#include <iostream>
#include <vector>

int genTriNum (int n) { return (n * (n + 1)) >> 1; };
int genSqNum  (int n) { return n * n; };
int genPentNum(int n) { return (n * (3 * n - 1)) >> 1; };
int genHexNum (int n) { return n * (2 * n - 1); };
int genHeptNum(int n) { return (n * (5 * n - 3)) >> 1; };
int genOctNum (int n) { return n * (3 * n - 2); };

std::vector<int> func(int, int, std::vector<std::vector<int>>&, int);

int setSize = 6;

int main() {
    std::vector<int> triNums, sqNums, pentNums, hexNums, heptNums, octNums;

    int n = 19;
    while (genTriNum(n) < 10000) {
        int figNum = genTriNum(n);
        if (figNum > 1000) triNums.push_back(figNum);

        figNum = genSqNum(n);
        if (figNum > 1000 && figNum < 10000) sqNums.push_back(figNum);

        figNum = genPentNum(n);
        if (figNum > 1000 && figNum < 10000) pentNums.push_back(figNum);

        figNum = genHexNum(n);
        if (figNum > 1000 && figNum < 10000) hexNums.push_back(figNum);

        figNum = genHeptNum(n);
        if (figNum > 1000 && figNum < 10000) heptNums.push_back(figNum);

        figNum = genOctNum(n);
        if (figNum > 1000 && figNum < 10000) octNums.push_back(figNum);

        n++;
    }

    std::vector<int> cycNums;
    std::vector<std::vector<int>> figNums = {triNums, sqNums, pentNums, hexNums, heptNums};
    for (int on: octNums) {
        cycNums = func(on, setSize, figNums, on / 100);
        if (!cycNums.empty()) break;
    }

    int sum = 0;
    for (int n : cycNums) {
        std::cout << n << " + ";
        sum += n;
    }
    std::cout << "\b\b= " << sum << std::endl;
}

std::vector<int> func(int n, int sz, std::vector<std::vector<int>>& figNums, int digs) {
    if (figNums.empty() && n % 100 == digs) return {n};

    int lDigs = n % 100;
    if (lDigs < 10) return {};

    std::vector<int> cycNums;
    for (size_t i = 0; i < figNums.size(); i++) {
        std::vector<std::vector<int>> nFigNums = figNums;
        nFigNums.erase(nFigNums.begin() + i);

        std::vector<int> nums;
        for (int m : figNums[i]) {
            int mDigs = m / 100;
            if (mDigs > lDigs) break;

            if (mDigs == lDigs) {
                nums = func(m, sz - 1, nFigNums, digs);
                nums.push_back(n);

                if (nums.size() == sz) cycNums = nums;
            }
        }
    }

    return cycNums;
}