/**
 * Finds the sum of all products whose multiplicand/multiplier/product triplets 
 *  can be written as 1 through 9 pandigital
 * Problem description at https://projecteuler.net/problem=32
 * 
 * Usage: PROGRAM
 * 
 * Notes:
 * - Any numbers containing a 0 cannot be the multiplicand, multiplier, or 
 *      product, because the final identity must be 1 to 9 pandigital
 * - Any numbers ending in a 1 cannot be multiplicand and multiplier, since
 *      there would be a repeated digit in the product
 * - Since an equation with a five-digit multiplicand would have at least a
 *      five-digit product, the multiplicand can be at most four digits (9876)
 * - The digit length of the multiplier must be 5 minus the length of the 
 *      multiplicand, as the product must be four digits
 * 
 * Expected Output: 45228
 */

#include <iostream>
#include "../helper.hpp"

int main() {
    std::unordered_set<int> panProds;
    for (int i = 9876; i >= 123; i--) {
        // Skips numbers with 0 or ending in 1
        if (i % 10 <= 1 || i > 100 && i % 100 < 10
                || i > 1000 && i % 1000 < 100) 
            continue;

        // Checks potential multipliers
        int nDigs = 5 - (floor(log10(i)) + 1);
        int min = pow(10, nDigs - 1);
        for (int j = min + 1; j < min * 10; j++) {
            if (j % 10 <= 1 || j > 100 && j % 100 < 10 
                    || j > 1000 && j % 1000 < 100) 
                continue;

            int p = i * j;
            if (floor(log10(p)) == 3    // Checks that p is 4 digits
                    && qst::isPandigital({i, j, p})) {
                panProds.emplace(p);
                // std::cout << i << " * " << j << " = " << p << "\n";
            }
        }
    }

    int sum = 0;
    for (int prod : panProds)
        sum += prod;

    std::cout << sum << std::endl;
}