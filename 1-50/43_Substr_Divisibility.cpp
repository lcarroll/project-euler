/**
 * Finds the sum of all 0-9 pandigital numbers whose three-digit substrings,
 *  starting with d2d3d4, are each divisible by one of the first seven prime 
 *  numbers
 * Problem description at https://projecteuler.net/problem=43
 *
 * Usage: PROGRAM
 *
 * Notes:
 * - Permutations with leading zeros can be excluded
 * - Permutations with this property will have
 *      - an even fourth digit (so d2d3d4 is divisible by 2)
 *      - digits d3, d4, and d5 whose sum is divisible by 3
 *      - a 5 as the sixth digit (so d4d5d6 is divisible by five and d6d7d8 can
 *          be divisible by 11)
 *
 * Expected Output: 16695334890
 */

#include <iostream>
#include <algorithm>
#include <math.h>

int main() {
    int primes[] = {2, 3, 5, 7, 11, 13, 17};
    int digits[] = {0, 9, 8, 7, 6, 5, 4, 3, 2, 1};

    long long sum = 0;
    while(std::next_permutation(digits, digits + 10)) {
        // Skips permutations with substrings not divisible by 2, 3, or 5
        if (digits[0] == 0 || digits[3] & 1 || digits[5] != 5) continue;
        if ((digits[2] + digits[3] + digits[4]) % 3 > 0) continue;

        // Checks substrings for divisibility by 7, 11, 13, and 17
        bool hasProp = true;
        for (int i = 4; i <= 7; i++) {
            int num = digits[i] * 100 + digits[i + 1] * 10 + digits[i + 2];
            if (num % primes[i - 1] > 0) {
                hasProp = false;
                break;
            }
        }

        if (hasProp) {
            for (int i = 0; i < 10; i++)
                sum += digits[i] * pow(10, 9 - i);
        }
    }

    std::cout << sum << std::endl;
}