/*
 * Finds the first n digits of a sum of numbers
 * Problem description at https://projecteuler.net/problem=13
 * 
 * Usage: PROGRAM [-f FILE] [-n NUMBER]
 * - FILE: The relative path to the file with the numbers to be added
 * - NUMBER: The number of digits of the sum to print (starting from the most 
 *      significant digit)
 * 
 * Notes:
 * - Has MPIR solution
 * 
 * Expected Output:
 * - I: 10, O: 5537376230
 */

#include <iostream>
#include <fstream>
#include "../helper.hpp"

void getNums(const std::string&, int&, std::vector<std::vector<char>>&);

int main(int argc, char** argv) {
    std::string file = "../input/p013_numbers.txt";
    int nDigs = 10;
    qst::checkInputs(argc, argv, file, nDigs);

    int nCols = 0;
    std::vector<std::vector<char>> nums;
    getNums(file, nCols, nums);
    int nRows = nums.size();

    std::vector<char> sum;
    int carry = 0;
    for (int i = 0; i < nCols; i++) {
        int colSum = carry;
        for (int row = 0; row < nRows; row++) {
            int index = nums[row].size() - 1 - i;
            colSum += (index >= 0) ? nums[row][index] : 0;
        }

        sum.push_back(colSum % 10);
        carry = colSum / 10;
    }

    // Converts vector to string
    std::string sumStr = (carry > 0) ? std::to_string(carry) : "";
    unsigned char cLen = sumStr.length();
    for (int i = 0; i < nDigs - cLen && i < sum.size(); i++)
        sumStr += sum[sum.size() - i - 1] + '0';

    std::cout << sumStr << std::endl;
}

// Reads in numbers from file
void getNums(const std::string &file, int &maxLen, std::vector<std::vector<char>> &nums) {
    std::ifstream in(file);

    std::string line;
    while (getline(in, line) && !line.empty()) {
        if (line.length() > maxLen) maxLen = line.length();

        std::vector<char> num;
        for (char dig : line)
            num.push_back(dig - '0');

        nums.push_back(num);
    }

    in.close();
}