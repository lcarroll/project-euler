/**
 * Finds the starting number up to some number with the longest Collatz sequence
 * Problem description at https://projecteuler.net/problem=14
 * 
 * Usage: PROGRAM [NUMBER]
 * - NUMBER: The upper bound of the sequence starting numbers
 * 
 * Notes:
 * - A number n up to half of the upper bound cannot have the longest sequence,
 *      as its double 2n will have a sequence length that is one longer
 * 
 * Expected Output:
 * - I: 1000000, O: 837799
 * 
 * Change Log:
 * - 2022-08-31 Switch to recursive function with unordered_map
 */

#include <iostream>
#include <unordered_map>
#include <string>

int getSeqLen(const long long, std::unordered_map<long long, int>&);

int main(int argc, char** argv) {
    int lim = (argc > 1) ? std::stoul(argv[1]) : 1000000;

    std::unordered_map<long long, int> numLenMap;
    numLenMap[1] = 1;

    int maxTerms = 0, seqStart = 0;
    for (int i = lim >> 1; i < lim; i++) {
        int len = getSeqLen(i, numLenMap);

        if (len > maxTerms) {
            maxTerms = len;
            seqStart = i;
        }
    }

    std::cout << "A chain starting at " << seqStart \
        << " has " << maxTerms << " terms" << std::endl;
}

int getSeqLen(const long long n, std::unordered_map<long long, int> &numLenMap) {
    if (numLenMap.count(n)) 
        return numLenMap.at(n);
    
    long long m = (n & 1) ? 3 * n + 1 : n >> 1;
    numLenMap[n] = getSeqLen(m, numLenMap) + 1;

    return numLenMap[n];
}