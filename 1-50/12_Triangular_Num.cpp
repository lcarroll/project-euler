/**
 * Finds the first triangular number with at least n divisors
 * Problem description at https://projecteuler.net/problem=12
 * 
 * Usage: PROGRAM [NUMBER]
 * - NUMBER: Target number of divisors
 * 
 * Notes:
 * - Odd triangle numbers can be skipped; evens are more divisible
 * - Only need to check for possible divisors up to the square root of a number
 *      (then multiply number of divisors by 2)
 * - euler also mentioned this solution: 
 *      https://mathschallenge.net/index.php?section=faq&ref=number/number_of_divisors
 * 
 * Expected Output:
 * - I: 5, O: 28
 * - I: 500, O: 76576500
 */

#include <iostream>
#include <string>
#include <math.h>

int main(int argc, char** argv) {
    int goal = (argc > 1) ? std::stoi(argv[1]) : 500;

    long long tNum = 1;
    long long i = 2;
    
    int numDivisors = 0;
    while (numDivisors <= goal) {
        // Get next even triangle number
        tNum += i++;
        while ((tNum & 1)) 
            tNum += i++;

        // Find divisors in range [1, sqrt(n))
        numDivisors = 2;
        int rt = sqrt(tNum);
        for (int i = 3; i <= rt; i++)
            if (tNum % i == 0) numDivisors++;  
        numDivisors <<= 1;

        // Check if integer part of square root is divisor
        if (rt * rt == tNum) numDivisors++;
    }
    
    std::cout << tNum << " : " << numDivisors << " divisors" << std::endl;
}