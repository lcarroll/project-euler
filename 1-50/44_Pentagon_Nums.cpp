/**
 * Finds the difference of the pair of pentagonal numbers whose sum and
 *  difference are also pentagonal and whose difference is minimized
 * Problem description at https://projecteuler.net/problem=44
 *
 * Usage: PROGRAM
 *
 * Notes:
 * - Can stop checking once the difference between consecutive pentagonal
 *      numbers exceeds the minimum difference found so far
 *
 * Expected Output: 5482660
 */

#include <iostream>
#include <math.h>

unsigned int getPentNum(int);
bool isPentNum(unsigned int);

int main() {
    unsigned int diff = UINT_MAX;
    // int t1 = 0, t2 = 0;

    // Checks diff against the difference between consecutive pentagonal numbers
    int n = 2;
    unsigned int pn1 = getPentNum(n);
    while (diff > pn1 - getPentNum(n - 1)) {
        for (int i = n - 1; i > 1; i--) {
            unsigned int pn2 = getPentNum(i);

            unsigned int d = pn1 - pn2;
            if (d > diff) break;

            if (isPentNum(d) && isPentNum(pn1 + pn2)) {
                diff = d;
                // t1 = n;
                // t2 = i;

                break;
            }
        }

        pn1 = getPentNum(++n);
    }

    std::cout << diff << std::endl;
}

unsigned int getPentNum(int n) {
    return (n * (3 * n - 1)) >> 1;
}

bool isPentNum (unsigned int p) {
    double n = (sqrt(24 * p + 1) + 1) / 6;

    if (floor(n) == ceil(n)) return true;
    else return false;
}
