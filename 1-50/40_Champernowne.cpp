/**
 * Finds the product of the digits at the positions n^0, n^1, ..., n^m in the
 *  fractional part of the irrational number 0.123456789101112...
 * Problem description at https://projecteuler.net/problem=40
 * 
 * Usage: PROGRAM [NUMBER] [NUMBER]
 * - (1) NUMBER: n
 * - (2) NUMBER: m
 * 
 * Expected Output:
 * - I: 10, 7; O: 210
 */

#include <iostream>
#include <string>
#include <math.h>

int main(int argc, char** argv) {
    int base = (argc > 1) ? std::stoul(argv[1]) : 10;
    int nPows = (argc > 2) ? std::stoul(argv[2]) : 7;

    int n = 1, nDigs = 0, p = 1;
    for (char i = 0; i < nPows; i++) {
        // Counts the number of digits until reaching/passing the next power
        while (nDigs < pow(base, i)) {
            nDigs += (int) (log10(n)) + 1;
            n++;
        }

        // Finds the digit of the most recent number in the desired position
        int m = n - 1;
        for (int j = nDigs - pow(base, i); j > 0; j--)
            m /= 10;

        std::cout << m % 10 << " * ";
        p *= m % 10;
    }

    std::cout << "\b\b= " << p << std::endl;
}