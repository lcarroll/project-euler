/**
 * Finds the largest palindrome made from the product of two n-digit numbers
 * Problem description at https://projecteuler.net/problem=4
 * 
 * Usage: PROGRAM [NUMBER]
 * - NUMBER: n, the number of digits
 * 
 * Notes:
 * - If m is always the lesser of two numbers n and m, then there is no need
 *      to continue checking for palindromes once n is set to some m for which a
 *      palindrome has been found
 * - etatsui's solution is interesting (https://projecteuler.net/thread=4)
 * 
 * Test Problem:
 * - I: 2, O: 9009 (99 * 91)
 * - I: 3, O: 906609
 */

#include <iostream>
#include <string>
#include "../helper.hpp"

int main(int argc, char** argv) {
    int nDigs = (argc > 1) ? std::stoi(argv[1]) : 3;

    int n = std::pow(10, nDigs) - 1;
    int m = n;
    int pal = 0, palM = 0;

    int min = std::pow(10, nDigs - 1) + 1;
    for (; n > palM; n--) {
        for (m = n; m > min; m--) {
            int p = n * m;

            if (p < pal) break;
            else if (qst::isPalindrome(p) && p > pal) {
                pal = p;
                palM = m;
            }
        }
    }

    std::cout << pal;
}