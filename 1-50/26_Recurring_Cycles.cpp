/**
 * Finds the value of d less than a number for which the length of the 
 *  recurring cycle in the decimal part of 1/d is maximized
 * Problem description at https://projecteuler.net/problem=26
 * 
 * Usage: PROGRAM [NUMBER]
 * - NUMBER: The number up to which to check for recurring cycles in 1/d
 * 
 * Notes:
 * - Can solve by simulating long division and checking for the same number to 
 *      come up again when getting remainders
 * - A cycle can be at most n - 1 digits long
 * - kelan notes that "the period of a number n's reciprocal is the smallest k 
 *      such that n divides 10^k - 1"
 * 
 * Expected Output:
 * - I: 10, O: 7
 * - I: 1000, O: 983
 * - I: 10000, O: 9967
 * - I: 100000, O: 99989
 * 
 * Change Log:
 * - 2022-09-01 Change loop to count down
 */

#include <iostream>
#include <unordered_map>
#include <string>
#include <math.h>

int main(int argc, char** argv) {
    int maxNum = (argc > 1) ? std::stoul(argv[1]) : 1000;

    int maxLen = 0, num = 0;
    for (int i = maxNum - 1; i > 1; i--) {
        if (maxLen > i) break;

        int div = pow(10, (int) log10(i - 1) + 1);

        int index = 0;
        std::unordered_map<int, int> divIndexMap;
        while (div > 0 && divIndexMap.count(div) == 0) {
            divIndexMap.emplace(div, index);
            div = (div % i) * 10;
            index++;
        }

        // Skips numbers with no cycle
        if (div == 0) continue;

        int diff = index - divIndexMap[div];
        if (div > 0 && diff > maxLen) {
            maxLen = diff;
            num = i;
        }
    }

    std::cout << num << " (cycle length of " << maxLen << ")\n";
}