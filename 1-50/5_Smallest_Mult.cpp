/**
 * Finds the smallest number evenly divisible by all numbers from 1 to n
 * Problem description at https://projecteuler.net/problem=5
 * 
 * Usage: PROGRAM [NUMBER] 
 * - NUMBER: The upper bound in the range [1, n]
 * 
 * Expected Output:
 * - I: 10, O: 2520
 * - I: 20, O: 232792560
 */

#include <iostream>
#include <vector>
#include <string>

int main(int argc, char** argv) {
    int sz = (argc > 1) ? std::stoul(argv[1]) : 20;
    sz--;
    
    std::vector<int> nums(sz);
    for (int i = 0; i < sz; i++) {
        nums[i] = i + 2;
    }

    // Finds prime factor(s) each number contributes to final product
    for (int i = 0; i < sz; i++) {
        for (int j = i + 1; j < sz; j++) {
            if (nums[j] % nums[i] == 0)
                nums[j] /= nums[i];
        }
    }

    unsigned long long p = 1;
    for (int i = 0; i < sz; i++) 
        p *= nums[i];

    std::cout << p << std::endl;
}