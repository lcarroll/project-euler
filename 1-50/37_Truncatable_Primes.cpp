/**
 * Finds the sum of the primes that are truncatable from left to right and from 
 *  right to left
 * Problem description at https://projecteuler.net/problem=37
 *
 * Usage: PROGRAM
 *
 * Definitions:
 * - Truncatable primes are those primes for which digits can be removed one at 
 *      a time from one end while the number remains prime
 *
 * Notes:
 * - Single-digit primes are not considered truncatable primes
 * - Truncatable primes cannot start or end with 1 or 9 or contain even digits
 *      (with the exception of a leading 2)
 * - Only eleven primes are truncatable in both directions
 *
 * Expected Output: 748317
 */

#include <iostream>
#include <set>
#include <math.h>

int truncLeft(int);

int main() {
    std::set<int> primes = {2, 3, 5, 7, 11, 13, 17, 19};

    auto isPrime = [&primes](int n) -> bool {
        if ((n & 1) == 0 && n > 2 || n < 2) return false;
        if (primes.count(n)) return true;

        int rt = ceil(sqrt(n));
        for (int prime : primes) {
            if (prime > rt) break;

            if (n % prime == 0) return false;
        }
        primes.emplace(n);

        return true;
    };

    int i = 21, sum = 0, nTruncPrimes = 0;
    while (nTruncPrimes < 11) {
        i += 2;
        if (i % 10 == 5 || i % 10 == 9)
            continue;

        if (isPrime(i)) {
            // Truncate right to left
            int n = i / 10;
            while (n > 0) {
                if (!isPrime(n)) break;
                n /= 10;
            }

            if (n > 0) continue;

            // Truncate left to right
            n = truncLeft(i);
            while (n > 0) {
                if (!isPrime(n)) break;
                n = truncLeft(n);
            }

            if (n == 0) {
                sum += i;
                nTruncPrimes++;

                // std::cout << i << "\n";
            }
        }
    }

    std::cout << sum << std::endl;
}

// Truncates a number from left to right
int truncLeft(int n) {
    int exp = log10(n);
    while (n >= pow(10, exp))
        n -= pow(10, exp);

    return n;
}