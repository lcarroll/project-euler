/**
 * Finds the sum of the digits in the number 2^n
 * Problem description at https://projecteuler.net/problem=16
 * 
 * Usage: PROGRAM [NUMBER]
 * - NUMBER: The exponent
 * 
 * Notes: 
 * - Has MPIR solution
 * 
 * Expected Output:
 * - I: 15, O: 26
 * - I: 1000, O: 1366
 */

#include <iostream>
#include <vector>
#include <string>

int main(int argc, char** argv) {
    int pow = (argc > 1) ? std::stoi(argv[1]) : 1000;
    
    std::vector<char> num = {1};
    for (size_t i = 0; i < pow; i++) {
        bool isCarry = 0;
        for (size_t j = 0; j < num.size(); j++) {
            char p = (num[j] << 1) + isCarry;
            num[j] = p % 10;
            isCarry = p > 9;            
        }
        if (isCarry) num.push_back(isCarry);
    }

    // for (int i = num.size() - 1; i > -1; i--)
    //     std::cout << (unsigned int) num[i];

    int sum = 0;
    for (char dig : num)
        sum += dig;
    std::cout << sum << std::endl;
}