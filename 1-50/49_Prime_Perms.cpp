/**
 * Finds the concatenated number of n primes in an arithmetic sequence that
 *  are permutations of one another
 * Problem description at https://projecteuler.net/problem=49
 *
 * Usage: PROGRAM [NUMBER] [NUMBER]
 * - NUMBER: The maximum number of digits the primes should have
 * - NUMBER: n
 *
 * Expected Output:
 * - I: 4, 3; O: 1487, 4817, 8147; 2969, 6299, 9629
 * - I: 5, 3; O: 54 solutions (see https://projecteuler.net/thread=49#3087)
 * - I: 5, 4; O: 83987, 88937, 93887, 98837
 * - I: 6, 4; O: 15 solutions (see https://projecteuler.net/thread=49#5075)
 * - I: 8, 5; O: 17460361, 31746061, 46031761, 60317461, 74603161
 */

#include <iostream>
#include <map>
#include <set>
#include "../helper.hpp"

void printSol(const std::set<int>&, int);

int main(int argc, char** argv) {
    int grpSize = 3, nDigs = 4;
    if (argc > 1) nDigs = std::stoul(argv[1]);
    if (argc > 2) grpSize = std::stoul(argv[2]);

    // Build sets of permutations of primes
    std::map<std::string, std::set<int>> permPrimeMap;
    for (int i = 1001; i < pow(10, nDigs); i++) {
        if (qst::isPrime(i)) {
            std::string str = std::to_string(i);
            std::sort(str.begin(), str.end());

            permPrimeMap[str].emplace(i);
        }
    }

    // Check permutations of given prime for series
    int cnt = 0;
    for (auto primeSet : permPrimeMap) {
        const std::set<int>& grp = primeSet.second;
        if (grp.size() < grpSize) continue;

        for (auto i = grp.begin(); i != grp.end(); i++) {
            int prime = *i;

            for (auto j = i; j != grp.end(); j++) {
                int perm = *j;
                if (perm <= prime) continue;

                std::set<int> series = {prime, perm};

                int diff = perm - prime;
                int num = perm + diff;
                while (grp.count(num)) {
                    series.emplace(num);
                    num += diff;
                }

                // Print series if solution
                if (series.size() == grpSize) {
                    std::cout << ++cnt << ". ";
                    printSol(series, diff);
                }
            }
        }
    }

    // for (int i = 1001; i < 3340; i++) {
    //     if (isPrime(i) && isPrime(i + 3330) && isPrime(i + 6660)
    //             && isPerm(i, i + 3330) && isPerm(i, i + 6660)) {
    //         std::cout << i << i + 3330 << i + 6660 << std::endl;
    //     }
    // }
}

void printSol(const std::set<int> &series, int diff) {
    for (int n : series)
        std::cout << n;

    std::cout << " (" ;
    for (int n : series) {
        std::cout << n;
        if (n < *(--series.end())) std::cout << ", ";
    }
    std::cout << "), diff = " << diff << "\n";
}
