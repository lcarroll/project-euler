/**
 * Adds all even-valued Fibonacci numbers below a number
 * Problem description at https://projecteuler.net/problem=2
 * 
 * Usage: PROGRAM [NUMBER]
 * - NUMBER: The number under which to add even-valued Fibonacci numbers
 * 
 * Expected Output:
 * - I: 4000000, O: 4613732
 */

#include <iostream>
#include <string>

int main(int argc, char** argv) {
    int max = (argc > 1) ? std::stoul(argv[1]) : 4000000;

    int pNum = 1, num = 1;
    int sum = 0;
    
    // Calculates next two odd numbers and adds them to sum
    while (num < max) {
        sum += pNum + num;

        pNum = pNum + 2 * num;
        num = 2 * pNum - num;
    }

    std::cout << sum << std::endl;
}