/**
 * Finds the sum of all numbers equal to the sum of the factorials of their 
 *  digits
 * Problem description at https://projecteuler.net/problem=34
 * 
 * Usage: PROGRAM
 * 
 * Notes:
 * - Any number for which this property holds must be less than 2540160, which is equal to 9! * 7
 * - 1 and 2 do not count, since 1 = 1 and 2 = 2 do not involve any addition
 * 
 * Expected Output: 40730
 */

#include <iostream>
#include <vector>

int main() {
    // Calculates and stores 1! through 9!
    int facts[10] {1, 1};
    for (int i = 2; i < 10; i++)
        facts[i] = facts[i - 1] * i;

    int sum = 0;
    for (int i = 10; i < 2540160; i++) {
        int num = i;

        int factSum = 0;
        while (num > 0) {
            factSum += facts[num % 10];
            if (factSum > i) break;

            num /= 10;
        }

        if (factSum == i) {
            sum += i;
            // std::cout << i << "\n";
        }
    }

    std::cout << sum << std::endl;
}