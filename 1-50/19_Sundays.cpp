/**
 * Counts the number of months that started on Sunday in the 20th century
 * Problem description at https://projecteuler.net/problem=19
 * 
 * Usage: PROGRAM
 * 
 * Expected Output: 171
 */

#include <iostream>

enum months {JAN, FEB, MAR, APR, MAY, JUN, JUL, AUG, SEP, OCT, NOV, DEC};

int main() {
    int month = JAN;
    int day = 366 % 7;
    int year = 1901;

    int nSuns = 0;
    while (year < 2001) {
        switch (month) {
            case JAN:
            case MAR:
            case MAY:
            case JUL:
            case AUG:
            case OCT:
            case DEC:
                day = (day + 31) % 7;
                break;
            case FEB:
                day += (year % 4 == 0) ? 29 : 28; 
                // && (year % 100 > 0 || year % 400 == 0))
                day %= 7;
                break;
            case APR:
            case JUN:
            case SEP:
            case NOV:
                day = (day + 30) % 7;
                break;
            default:
                break; 
        }

        if (day == 0) nSuns++;

        month = (month + 1) % 12;
        if (month == JAN) year++;
    }

    std::cout << nSuns << std::endl;
}