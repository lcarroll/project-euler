/**
 * Finds the largest n-digit 1 to n pandigital prime
 * Problem description at https://projecteuler.net/problem=41
 *
 * Usage: PROGRAM
 *
 * Notes:
 * - Since all eight- and nine-digit pandigital numbers are divisible by 3, the 
 *      greatest pandigital prime must be at most seven digits
 * - Solution with string assumes a seven-digit pandigital prime exists
 *
 * Expected Output: 7652413
 */

#include <iostream>
#include "../helper.hpp"

int main() {
    std::string digits = "7654321";
    while (!qst::isPrime(std::stoi(digits)))
        std::prev_permutation(digits.begin(), digits.end());

    std::cout << digits << std::endl;

    int n = 7654321;
    while (!qst::isPrime(n) || !qst::isPandigital(n, 1, (int) log10(n) + 1))
        n -= 2;

    std::cout << n << std::endl;
}