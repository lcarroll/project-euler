/**
 * Sorts a list of names, assigning each a value based on their characters and 
 *  their position in the sorted list
 * Problem description at https://projecteuler.net/problem=22
 * 
 * Usage: PROGRAM [FILE]
 * - FILE: The file containing the list of names
 * 
 * Restrictions:
 * - Names are assumed to be in uppercase and in quotes
 * 
 * Expected Output: 871198282
 */

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <algorithm>

std::vector<std::string> getNames(const std::string&);

int main(int argc, char** argv) {
    std::string file = "../input/p022_names.txt";
    if (argc > 1) file = argv[1];

    std::vector<std::string> names = getNames(file);
    std::sort(names.begin(), names.end());

    long long total = 0;
    for (int i = 0; i < names.size(); i++) {
        int alphaVal = 0;
        for (char letter : names[i])
            alphaVal += letter - 'A' + 1;
        total += alphaVal * (i + 1);
    }

    // std::string sFile = "sorted_" + file;
    // if (argc > 2) sFile = argv[2];

    // std::string cmd = "powershell -command \"(cat ";
    // cmd.append(file).append(") -split \\\",\\\" | sort > ").append(sFile);
    // system(&cmd[0]);

    // std::ifstream in(sFile);

    // int lineNum = 1;
    // int total = 0;
    // while (std::getline(in, name)) {
    //     int alphaVal = 0;
    //     for (char letter : name)
    //         alphaVal += (letter >= 'A' && letter <= 'Z') ? letter - 'A' + 1 : 0;

    //     total += alphaVal * lineNum;
    //     lineNum++;
    // }

    // in.close();

    std::cout << total << std::endl;
}

std::vector<std::string> getNames(const std::string& file) {
    std::ifstream in(file);
    std::vector<std::string> names;

    std::string namesList;
    while (getline(in, namesList)) {
        std::istringstream iss(namesList);
        std::string name;
        while(getline(iss, name, ',')) 
            names.push_back(name.substr(1, name.length() - 2));
    }

    in.close();

    return names;
}