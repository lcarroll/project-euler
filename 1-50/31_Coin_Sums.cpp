/**
 * Finds the number of ways to make £2 with any number of coins in the 
 *  following denominations: 1p, 2p, 5p, 10p, 20p, 50p, £1 (100p), and £2 (200p)
 * Problem description at https://projecteuler.net/problem=31 
 * 
 * Usage: PROGRAM [NUMBER]
 * - NUMBER: The amount to make change for (in pence)
 * 
 * Notes:
 * - Can be solved by subtracting the value of each coin smaller than the 
 *      target value from the target value and solving for the difference
 * - Can be solved with recursion and/or memoization
 * 
 * Expected Output:
 * - I: 200, O: 73682
 */

#include <iostream>
#include <vector>
#include <string>
#include <functional>

int main(int argc, char** argv) {
    int goal = (argc > 1) ? std::stoul(argv[1]) : 200;

    const int coins[] = {200, 100, 50, 20, 10, 5, 2, 1};

    // Solves with recursion
    std::function<int (int, int)> calcNumWays;
    calcNumWays = [&coins, &calcNumWays](int n, int d) -> int {
        int nWays = 1;
        for (int i = d; i < 7; i++) {
            if (n - coins[i] == 0) nWays++;
            if (n - coins[i] > 0) nWays += calcNumWays(n - coins[i], i);
        }
        
        return nWays;
    };

    std::cout << calcNumWays(goal, 0) << std::endl;

    // Solves with memoization
    std::vector<int> nWays(goal + 1, 0);
    nWays[0] = 1;
    for (int i = 7; i >= 0; i--) {
        for (int j = coins[i]; j <= goal; j++)
            nWays[j] += nWays[j - coins[i]];
    }

    std::cout << nWays[goal] << std::endl;
}