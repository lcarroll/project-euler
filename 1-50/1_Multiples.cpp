/** 
 * Finds and adds all multiples of 3 or 5 below a number
 * Problem description at https://projecteuler.net/problem=1
 * 
 * Usage: PROGRAM [NUMBER]
 * - NUMBER: The number up to which to check for multiples
 * 
 * Expected Output:
 * - I: 10, O: 23
 * - I: 1000, O: 233168
 */

#include <iostream>
#include <string>

int main(int argc, char** argv) {
    int max = 1000;
    if (argc > 1) max = std::stoi(argv[1]);

    int sum = 0;
    for (int i = 3; i < max; i += 3)
        sum += i;
    for (int i = 5; i < max; i += 5)
        if (i % 3 > 0) sum += i;

    std::cout << sum << std::endl;
}