/**
 * Finds all primes below a given number that are circular primes
 * Problem description at https://projecteuler.net/problem=35
 *
 * Usage: PROGRAM [NUMBER]
 * - NUMBER: The number up to which to look for circular primes
 *
 * Definitions: 
 * - Circular primes are primes whose rotations are also prime
 *
 * Notes:
 * - Single-digit primes count as circular primes
 * - Circular primes above 10 cannot have digits other than 1, 3, 7, and 9
 *
 * Expected Output:
 * - I: 100, O: 13
 * - I: 1000000, O: 55
 *
 * Change Log:
 * - 2022-09-07 Switch to using unordered_set to track primes
 */

#include <iostream>
#include <unordered_set>
#include <math.h>
#include "../helper.hpp"

int main(int argc, char** argv) {
    int max = (argc > 1) ? std::stoul(argv[1]) : 1000000;

    std::unordered_set<int> circPrimes {2, 3, 5, 7};
    for (int i = 11; i < max; i += 2) {
        if (circPrimes.count(i) || !qst::isPrime(i))
            continue;

        int n = i;
        int log = log10(n);

        // Checks if each rotation of i is prime
        std::unordered_set<int> rots {i};
        do {
            n = (n % 10) * pow(10, log) + n / 10;
            rots.emplace(n);
        } while (n != i && (n & 1) == 1 && qst::isPrime(n));

        if (n == i) circPrimes.insert(rots.begin(), rots.end());
    }

    std::cout << circPrimes.size() << std::endl;
}