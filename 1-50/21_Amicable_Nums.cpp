/**
 * Finds the sum of all amicable numbers under a number
 * Problem description at https://projecteuler.net/problem=21
 * 
 * Usage: PROGRAM [NUMBER]
 * - NUMBER: The number under which to sum all amicable numbers
 * 
 * Definitions:
 * - Amicable numbers are pairs of numbers for which the sum of the proper 
 *      divisors of each one equals the other
 * 
 * Expected Output:
 * - I: 10000, O: 31626
 */

#include <iostream>
#include <unordered_map>
#include "../helper.hpp"

int main (int argc, char** argv) {
    int max = (argc > 1) ? std::stoi(argv[1]) : 10000;

    std::unordered_map<int, int> pairs;

    int sum = 0;
    for (int i = 5; i < max; i++) {
        int di = qst::d(i);

        if (di != i && pairs.count(di) && pairs.at(di) == i) {
            sum += di + i;
            // std::cout << i << " and " << sum << "\n";
        } else if (di > i) pairs[i] = di;
    }

    std::cout << sum << std::endl;
}