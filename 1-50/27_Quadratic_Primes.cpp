/**
 * Finds the product of the coefficients a and b (with absolute values less
 *  than or equal to a number) in the equation n^2 + an + b that make up the
 *  quadratic expression that generates the most consecutive (positive) primes 
 *  for values of n starting with 0
 * Problem description at https://projecteuler.net/problem=27
 * 
 * Usage: PROGRAM [NUMBER]
 * - NUMBER: The maximum absolute value of b (a's is NUMBER - 1)
 * 
 * Notes:
 * - An expression with a b-value that is not a positive prime will never yield 
 *      a positive prime when n = 0
 * - a and b must be odd, since an even a would alternately require an odd and 
 *      an even b with consecutive values of n
 * - The value of b limits the number of solutions
 *      - When n = b, n^2 + an + b = b^2 + ab + b = b(b + a + 1)
 *      - b(b + a + 1) is a multiple of b and therefore not a prime
 * - a and b must be at least maxCnt apart
 *      - expression can be factored: n(n + a) + b
 *      - If n = b - a, b = n + a, and n(n + a) + b = nb + b
 *      - nb + b = b(n + 1), which is a multiple of b and therefore not a prime
 * 
 * Expected Output:
 * - I: 1000, O: -59231
 * - I: 1601, O: −126479
 */

#include <iostream>
#include <unordered_set>
#include <math.h>
#include "../helper.hpp"

int main(int argc, char** argv) {
    int lim = (argc > 1) ? std::stoul(argv[1]) : 1000;
    std::unordered_set<unsigned int> primes = {2};

    auto isPrime = [&primes](int n) {
        // if ((n & 1) == 0 || n < 2) return false;
        if (primes.count(n)) return true;

        bool isPrimeN = qst::isPrime(n);
        if (isPrimeN) primes.emplace(n);
        
        return isPrimeN;
    };

    int p = 41;
    int maxCnt = 40;
    for (int a = (lim & 1) ? -lim : -lim + 1; a < lim; a += 2) {
        for (int b = (lim & 1) ? lim : lim - 1; b > maxCnt; b -= 2) {
            if (b - a < maxCnt   // n(n + a) + b => n = b - a
                || !isPrime(maxCnt * maxCnt + a * maxCnt + b))
                continue;

            int nPrimes = 0;
            int n = 0;
            while (isPrime(n * n + a * n + b)) {
                nPrimes++;
                n++;
            }

            if (nPrimes > maxCnt) {
                maxCnt = nPrimes;
                p = a * b;
            }
        }
    }

    std::cout << p << " (" << maxCnt << " primes)\n";
}