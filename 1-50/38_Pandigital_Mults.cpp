/**
 * Finds the greatest 1 to 9 pandigital number that can be formed as the concatenated product of a number and consecutive integers starting with 1
 * Problem description at https://projecteuler.net/problem=38
 * 
 * Usage: PROGRAM
 * 
 * Notes:
 * - The concatenated product of 9 and (1, 2, 3, 4, 5) is 918273645 (given in 
 *      the problem statement), making it the number to beat
 * - The number must start with a 9, else the concatenated product of 9x, where 
 *      x represents any number of digits, and (1, 2, ..., n) would not start 
 *      with a 9
 * (The following with credit to user gamma)
 * - The number must be a four-digit number 9xyz, as the concatenated product 
 *      of 9xyz and (1, 2) would be 4 + 5 = 9 digits
 *      - The concatenated product of a two-digit number 9x or a three-digit 
 *          number and (1, 2, ..., n) would never be nine digits
 *      - The concatenated product of numbers with more digits and (1, 2) would 
 *          always be too large
 *      - The concatenated product of a number and (1) is considered trivial
 * - The number must contain all different digits (not 0), or the concatenated 
 *      product would contain multiple of the same digit (and it must be a 
 *      permutation of the digits 1-9)
 * - The second digit in the number cannot be higher than 3, or the 
 *      concatenated product would be assured to have multiple 8s or 9s
 * - The number cannot contain a 1 or an 8, or the concatenated product would 
 *      contain multiple 1s or 8s, respectively
 * - The concatenated product of a number 9xyz that follows the rules above and
 *      (1, 2) will take the form 9xyz18abc
 *
 * Expected Output: 932718654
 */

#include <iostream>
#include "../helper.hpp"

int main() {
    for (int i = 9376; i >= 9234; i--) {
        if (i % 5 == 0) continue;
        
        if (qst::isPandigital({i, i * 2})) {
            std::cout << i << i * 2 << std::endl;
            return 0;
        }
    }
}
