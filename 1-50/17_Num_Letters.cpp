/**
 * Counts the number of letters in the spelled-out numbers from 1 to n
 * Problem description at https://projecteuler.net/problem=17
 * 
 * Usage: PROGRAM [NUMBER]
 * - NUMBER: The highest number to write out in words
 * 
 * Notes:
 * - A 1 will always contribute three letters ("one" or "ten")
 * - A 2 will contribute either three or six letters ("two" or "twenty")
 * - A 3 will contribute either five or six letters ("three" or "thirty")
 * - A 4 will contribute either four or five letters ("four" or "forty")
 * - A 5 will contribute either four or five letters ("five" or "fifty")
 * - A 6 will contribute either three or five letters ("six" or "sixty")
 * - A 7 will contribute either five or seven letters ("seven" or "seventy")
 * - An 8 will contribute either five or six letters ("eight" or "eighty")
 * - A 9 will contrbute either four or six letters ("nine" or "ninety")
 * - A 0 will contribute nothing if the places of the digits are determined
 * - It is sufficient to rely on the rules above when determining letter counts 
 *      for numbers between 10 and 20; simply add 1 for 14, 16, 17, and 19
 * 
 * Expected Output:
 * - I: 5, O: 19
 * - I: 1000, O: 21124
 */

#include <iostream>
#include <string>

int main(int argc, char** argv) {
    int max = (argc > 1) ? std::stoi(argv[1]) : 1000;

    if (max > 1000)
        std::cout << "Be aware that this program is not guaranteed to yield correct results for numbers greater than 1000\n";

    int sum = 0;
    for (int i = 1; i <= max; i++) {
        std::string num = std::to_string(i);
        int nDigs = num.length();
        
        for (int j = 0; j < nDigs; j++) {
            int d = nDigs - j;
            switch (num[j]) {
                case '1':
                    sum += 3;

                    sum += (d % 3 == 2 && (num[j + 1] == '4' || 
                        num[j + 1] == '6' || num[j + 1] == '7' || 
                        num[j + 1] == '9')) ? 1 : 0;

                    break;
                case '2':
                    sum += (d % 3 != 2) ? 3 : 6;
                    break;
                case '3':
                case '8':
                    sum += (d % 3 != 2) ? 5 : 6;
                    break;
                case '4':
                case '5':
                    sum += (d % 3 != 2) ? 4 : 5;
                    break;
                case '6':
                    sum += (d % 3 != 2) ? 3 : 5;
                    break;
                case '7':
                    sum += (d % 3 != 2) ? 5 : 7;
                    break;
                case '9':
                    sum += (d % 3 != 2) ? 4 : 6;
                    break;
                default:
                    break;
            }
        }

        int n = i;
        while (n >= 100) {
            int nm = n % 1000;

            if (nm >= 100) sum += 7;
            if (nm % 100 > 0) sum += 3;

            n /= 1000;
        }

        if (i / 1000 % 1000 > 0) sum += 8;
    }

    std::cout << sum << std::endl;
}