/**
 * Finds the number of routes through a square grid from the one corner to the 
 *  opposite corner
 * Problem description at https://projecteuler.net/problem=15
 * 
 * Usage: PROGRAM [NUMBER]
 * - NUMBER: The number of cells along one side of the grid
 * 
 * Notes:
 * - May be solved using Pascal's Triangle and combinations
 * 
 * Expected Output:
 * - I: 2, O: 6
 * - I: 20, O: 137846528820
 */

#include <iostream>
// #include <vector>
#include <string>

int main(int argc, char** argv) {
    int sz = (argc > 1) ? std::stoul(argv[1]) : 20;

    long long numPaths = 1 << ((sz + 1) >> 1);
    for (int i = (sz << 1) - 1; i > sz; i -= 2)
        numPaths *= i;
    for (int i = 2; i <= sz >> 1; i++)
        numPaths /= i;

    std::cout << numPaths << std::endl;

    // std::vector<std::vector<long long>> gPnts(sz + 1, std::vector<long long>(sz + 1, 0));
    // for (int i = 1; i <= sz; i++) {
    //     gPnts[i][0] = 1;
    //     gPnts[0][i] = 1;
    // }

    // for (int i = 1; i <= sz; i++) {
    //     for (int j = 1; j <= sz; j++) {
    //         gPnts[i][j] = gPnts[i][j - 1] + gPnts[i - 1][j];
    //     }
    // }

    // std::cout << gPnts[sz][sz] << std::endl;
}