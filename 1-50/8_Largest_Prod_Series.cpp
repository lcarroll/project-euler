/**
 * Finds the n adjacent digits in a number with the greatest product
 * Problem description at https://projecteuler.net/problem=8
 * 
 * Usage: PROGRAM [-f FILE] [-n NUMBER]
 * - FILE: File containing the number to be read in
 * - NUMBER: The number of adjacent digits
 * 
 * Notes:
 * - Can stop a calculation and slide the window on encountering a 0
 * 
 * Expected Output:
 * - I: 4, O: 5832
 * - I: 13, O: 23514624000
 */

#include <iostream>
#include <fstream>
#include <vector>
#include "../helper.hpp"

std::vector<char> readNum(const std::string&);

int main(int argc, char** argv) {
    int winSz = 13;
    std::string file = "../input/p008_number.txt";
    qst::checkInputs(argc, argv, file, winSz);

    std::vector<char> num = readNum(file);

    int index = winSz;
    unsigned long long p = 0, max = 0;
    while (index < num.size()) {
        if (p == 0) {   // Calculate product of winSz numbers after a 0 
            p = 1;
            for (int i = index - winSz; i < index; i++) {
                p *= num[i];
                if (p == 0) {
                    index = i + winSz + 1;
                    break;
                }
            }
        } else {        // Shift window by one
            p /= num[index - winSz];
            p *= num[index++];  
        }

        if (p > max) max = p;
    }

    std::cout << max;
}

// Reads large number from file and stores it in vector
std::vector<char> readNum(const std::string &file) {
    std::vector<char> num;

    std::ifstream in(file);

    std::string line;
    while (std::getline(in, line)) {
        for (char dig : line) 
            num.push_back(dig - '0');
    }

    return num;
}