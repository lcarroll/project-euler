/**
 * Finds the prime under some max that can be written as the sum of the most
 *  consecutive primes
 * Problem description at https://projecteuler.net/problem=50
 *
 * Usage: PROGRAM [NUMBER]
 * - NUMBER: The number up to which to look for primes
 *
 * Notes:
 * - Can calculate the length of the chain each prime starts
 *
 * Expected Output:
 * - I: 100, O: 41 (6 primes)
 * - I: 1000, O: 953 (21 primes)
 * - I: 1000000, O: 997651 (543 primes)
 *
 * Change Log:
 * - 2022-09-15 Change in algorithm
 */

#include <iostream>
#include <vector>
#include <string>
#include <math.h>

int main(int argc, char** argv) {
    int max = 1000000;
    if (argc > 1) max = std::stoi(argv[1]);

    std::vector<int> primes = {2};

    auto isPrime = [&primes](int n) -> bool {
        if ((n & 1) == 0 && n > 2) return false;

        int rt = sqrt(n);
        for (int i = 1; i < primes.size() && primes[i] <= rt; i++)
            if (n % primes[i] == 0) return false;

        return true;
    };

    // Generates primes under max
    for (int i = 3; i < max; i += 2) {
        if (!isPrime(i)) continue;
        primes.push_back(i);
    }

    // Finds the longest chain starting with each prime
    int maxPrimes = 0, prime = 2, startPrime = 2;
    for (int i = 0; i < primes.size() - maxPrimes; i++) {
        int sum = primes[i];
        for (int j = i + 1; j < primes.size(); j++) {
            sum += primes[j];
            if (sum > max) break;

            if ((j - i + 1) > maxPrimes && isPrime(sum)) {
                startPrime = primes[i];

                maxPrimes = j - i + 1;
                prime = sum;
            }
        }
    }

    // for (int i = primes.size() - 1; i >= idx; i--) {
    //     if (maxPrimes > i) break;

    //     int sum = 0;
    //     int first = 0, last = 0;
    //     while (sum < primes[i]) {
    //         sum += primes[last++];
    //         while (sum > primes[i])
    //             sum -= primes[first++];
    //     }

    //     int nConPrimes = last - first;
    //     if (nConPrimes > maxPrimes) {
    //         maxPrimes = nConPrimes;
    //         prime = primes[i];
    //     }
    // }

    std::cout << prime << " (" << maxPrimes << " primes, starting at " \
        << startPrime << ")\n";
}