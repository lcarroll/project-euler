/**
 * Finds the smallest odd composite number that cannot be written as the sum of
 *  a prime and twice a square
 * Problem description at https://projecteuler.net/problem=46
 *
 * Usage: PROGRAM
 *
 * Expected Output: 5777
 */

#include <iostream>
#include "../helper.hpp"

bool fitsConjecture(int);

int main() {
    int n = 35;
    while (fitsConjecture(n))
        n += 2;

    std::cout << n << std::endl;
}

bool fitsConjecture(int n) {
    if (qst::isPrime(n)) return true;

    for (int i = 1; 2 * i * i < n; i++) {
        if (qst::isPrime(n - 2 * i * i)) return true;
    }

    return false;
}
