/**
 * Finds the sum of all numbers below some maximum that are palindromes in both
 *  binary and decimal
 * Problem description at https://projecteuler.net/problem=36
 *
 * Usage: PROGRAM [NUMBER]
 * - NUMBER: The number up to which to check for palindromic numbers
 *
 * Notes:
 * - Without leading zeros, even numbers are not palindromes in binary
 *
 * Expected Output:
 * - I: 1000000, O: 872187
 */ 

#include <iostream>
#include <math.h>
#include "../helper.hpp"

int main(int argc, char** argv) {
    int max = (argc > 1) ? std::stoul(argv[1]) : 1000000;

    unsigned int sum = 25;
    for (int i = 11; i < max; i += 2) {
        if (!qst::isPalindrome(i)) continue;

        int j = 1 << (int) log2(i);
        
        std::string bNum = "";
        for (; j >= 1; j >>= 1) {
            bNum += (i & j) ? '1' : '0';
        }

        if (qst::isPalindrome(bNum)) sum += i;
    }

    std::cout << sum << std::endl;
}