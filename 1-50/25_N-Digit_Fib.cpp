/**
 * Finds the index of the first term in the Fibonacci sequence with n digits
 * Problem description at https://projecteuler.net/problem=25
 * 
 * Usage: PROGRAM [NUMBER]
 * - NUMBER: The desired number of digits
 * 
 * Notes:
 * - Has MPIR solution
 * 
 * Expected Output:
 * - I: 3, O: 12
 * - I: 1000, O: 4782
 */

#include <iostream>
#include <vector>
#include <string>

int main(int argc, char** argv) {
    int nDigs = (argc > 1) ? std::stoul(argv[1]) : 1000;

    std::vector<char> n1 = {1};
    std::vector<char> n2 = {2};
    int index = 3;

    char carry = 0;
    while (n2.size() < nDigs) {
        carry = 0;    
        for (size_t i = 0; i < n1.size(); i++) {
            char sum = n1[i] + n2[i] + carry;
            n1[i] = sum % 10;
            carry = sum / 10;
        }
        
        if (n2.size() > n1.size()) {
            char sum = n2[n2.size() - 1] + carry;
            n1.push_back(sum % 10);
            carry = sum / 10;
        }
        if (carry > 0) n1.push_back(carry);

        n1.swap(n2);
        index++;
    }

    std::cout << index << std::endl;
}