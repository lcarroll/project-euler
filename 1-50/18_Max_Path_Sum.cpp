/**
 * Finds the maximum total from top to bottom of a triangle
 * Problem description at https://projecteuler.net/problem=18
 * 
 * Usage: PROGRAM [FILE]
 * - FILE: The file containing the triangle
 * 
 * Notes:
 * - Can be accomplished by: 
 *      - Reading in one row at a time
 *      - Collapsing triangle by adding each number in a row with the greater
 *          of the two numbers neighboring it in the row above
 *      - Searching final row for greatest number
 * 
 * Expected Output:
 * - O: 23, where the triangle is 
 *      3
 *      7 4
 *      2 4 6
 *      8 5 9 3
 * - O: 1074, where the triangle is given by the input file
 */

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>

std::vector<int> getRow(std::string&);

int main(int argc, char** argv) {
    std::string fileName = (argc > 1) ? argv[1] : "../input/p018_triangle.txt";

    std::ifstream in(fileName);
    std::string line;
    std::getline(in, line);

    std::vector<int> pRow, row;
    pRow = getRow(line);

    while (std::getline(in, line)) {
        row = getRow(line);

        for (int j = 1; j < pRow.size(); j++)
            row[j] += (pRow[j] > pRow[j - 1]) ? pRow[j] : pRow[j - 1];

        pRow = row;
    }

    in.close();

    int max = 0;
    for (int num : row)
        if (num > max) max = num;

    std::cout << max << std::endl;
}

std::vector<int> getRow(std::string& line) {
    std::istringstream iss(line);

    // Adds zeros to both ends of a line to prevent having to check for edges
    std::vector<int> row = {0};
    int num;
    while (iss >> num) 
        row.push_back(num);
    row.push_back(0);

    return row;
}