/**
 * Finds the first of n consecutive numbers with n distinct prime factors each
 * Problem description at https://projecteuler.net/problem=47
 *
 * Usage: PROGRAM [NUMBER]
 * - NUMBER: n
 *
 * Notes:
 * - Even numbers will have the same number of prime factors as their half if
 *      their half is even or one more prime factor if their half is odd
 * - Odd numbers will have the same number of prime factors as one of its
 *      factors if it and its factor are divisible by the same prime
 *
 * Expected Output:
 * - I: 2, O: 14
 * - I: 3, O: 644
 * - I: 4, O: 134043
 * - I: 5, O: 129963314
 *
 * Change Log:
 * - 2022-09-15 Switch to use memoization
 */

#include <iostream>
#include <vector>
#include <string>
#include <math.h>

int main(int argc, char** argv) {
    int nFactors = 4;
    if (argc > 1) nFactors = std::stoul(argv[1]);

    int n = 8;
    std::vector<int> factorCnt {0, 0};
    auto countPrimeFactors = [&factorCnt](int n) -> int {
        int m = factorCnt.size();

        int factors = 1;
        while (m <= n) {
            if ((m & 1) == 0) {
                factors = (m & 2) ? factorCnt[m >> 1] + 1 : factorCnt[m >> 1];
                factorCnt.push_back(factors);
            } else {
                factors = 1;
                for (int i = 3; i <= sqrt(m); i += 2) {
                    if (m % i == 0 && (m / i) % i == 0) {
                        factors = factorCnt[m / i];
                        break;
                    } else if (m % i == 0) {
                        factors = factorCnt[m / i] + 1;
                        break;
                    }
                }
                factorCnt.push_back(factors);
            }

            m++;
        }

        return factors;
    };

    bool isFound = false;
    while (!isFound) {
        for (int i = n; i < n + nFactors; i++) {
            isFound = true;
            if (countPrimeFactors(i) != nFactors) {
                isFound = false;
                n += (i - n) + 1;
                break;
            }
        }
    }

    std::cout << n << std::endl;
}
