/**
 * Finds the last ten digits of the series 1^1 + 2^2 + ... + n^n
 * Problem description at https://projecteuler.net/problem=48
 *
 * Usage: PROGRAM [NUMBER]
 * - NUMBER: n
 *
 * Notes:
 * - The last 10 digits of self powers of numbers divisible by 10 will all be 0
 *
 * Expected Output:
 * - I: 10, O: 0405071317
 * - I: 1000, O: 9110846700
 */

#include <iostream>
#include <string>
#include <math.h>

int main(int argc, char** argv) {
    int last = 1000;
    if (argc > 1) last = std::stoul(argv[1]);

    unsigned long long n = 1;

    for (int i = 2; i <= last; i++) {
        if (i % 10 == 0) continue;

        unsigned long long p = i * i;
        for (int j = 3; j <= i; j++) 
            p = (p * i) % 10000000000;
        n += p;
    }

    n %= 10000000000;
    for (int i = 10; i > (int) log10(n) + 1; i--)
        std::cout << "0";

    std::cout << n << std::endl;
}