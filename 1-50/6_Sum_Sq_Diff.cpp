/**
 * Finds the difference between the square of the sum and the sum of the 
 *  squares of integers from 1 to n
 * Problem description at https://projecteuler.net/problem=6
 * 
 * Usage: PROGRAM [NUMBER]
 * - NUMBER: The upper bound in the range [1, n]
 * 
 * Notes:
 * - (1 + 2 + ... + n) = n * (n + 1) / 2
 * - 1^2 + 2^2 + ... + n^2 = n * (n + 1) * (2n + 1) / 6
 * 
 * Expected Output:
 * - I: 10, O: 2640
 * - I: 100, O: 25164150
 */

#include <iostream>
#include <string>

int main(int argc, char** argv) {
    int n = (argc > 1) ? std::stoul(argv[1]) : 100;

    int sum = n * (n + 1) / 2;
    int sumSquares = n * (n + 1) * (2 * n + 1) / 6;
    // for (int i = 1; i <= n; i++) {
    //     sum += i;
    //     sumSquares += i * i;
    // }

    std::cout << sum * sum - sumSquares << std::endl;
}