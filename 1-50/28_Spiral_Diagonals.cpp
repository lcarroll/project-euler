/**
 * Finds the sum of the numbers in the diagonals of an n x n spiral
 * Problem description at https://projecteuler.net/problem=28
 * 
 * Note that the spiral is created by spiraling clockwise outward from 1
 * 
 * Expected Output:
 * - I: 5, O: 101
 * - I: 1001, O: 669171001
 */

#include <iostream>
#include <string>

int main(int argc, char** argv) {
    int sq = (argc > 1) ? std::stoul(argv[1]) : 1001;

    long long sum = 1;
    for (int i = 3; i <= sq; i += 2)
        sum += 4 * i * i - 6 * i + 6;

    // sq *= sq;
    // for (int i = 0, n = 1, inc = 0; n <= sq; i++, n += inc) {
    //     if ((i & 3) == 0) inc += 2;
    //     sum += n;
    // }

    std::cout << sum << std::endl;
}