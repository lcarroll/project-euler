/**
 * Finds the perimeter p of a right triangle up to some number with the maximum number of integer solutions (a, b, c)
 * Problem description at https://projecteuler.net/problem=39
 *
 * Usage: PROGRAM [NUMBER]
 * - NUMBER: The maximum right triangle perimeter
 *
 * Notes:
 * - The smallest integer solution is (3, 4, 5) for perimeter 12
 * - If a is the smallest number in the solution, a will always be less than 
 *      one-third the perimeter
 * - c must be the greatest number in the solution
 *
 * Expected Output:
 * - I: 1000, O: 840
 */

#include <iostream>
#include <string>

int main(int argc, char** argv) {
    int max = (argc > 1) ? std::stoul(argv[1]) : 1000;

    int p = 120, maxSols = 3;   // Given in the problem statement
    for (int i = 12; i <= max; i += 2) {
        int numSols = 0;
        for (int a = 1; a < i / 3; a++) {
            for (int b = a + 1; b < i - a; b++) {
                int c = i - a - b;
                if (c < b) break;

                if (a * a + b * b == c * c)
                    numSols++;
            }
        }

        if (numSols > maxSols) {
            maxSols = numSols;
            p = i;
        }
    }

    std::cout << "Perimeter " << p << " (" << maxSols << " solutions)\n";
}