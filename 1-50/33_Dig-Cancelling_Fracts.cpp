/**
 * Finds the reduced denominator of the product of the four fractions with two 
 *  digits in both numerator and denominator that can be correctly "reduced" by 
 *  crossing out the matching digits in opposite positions in the numerator and 
 *  denominator
 * Problem description at https://projecteuler.net/problem=33
 * 
 * Usage: PROGRAM
 * 
 * Notes:
 * - 49/98 is one of these fractions, but 30/50 is not, as the zeros are both 
 *      in the ones place
 * - These fractions do not have to be fully reduced, e.g. 49/98 "reduces" to
 *      4/8 and not 1/2
 * The four fractions take the form xy/yz
 * 
 * Expected Output: 100
 */

#include <iostream>
#include <math.h>
#include "../helper.hpp"

int main() {
    int num = 1, den = 1;

    for (int n = 11; n < 90; n++) {
        if (n % 10 == 0) continue;

        int mod10 = n % 10;
        int div10 = n / 10;

        // Skips improper fractions
        if (div10 > mod10) continue;

        // Checks fractions of the form xy/yz
        int start = mod10 * 10 + div10 + 1;
        for (int d = start; d < start + (10 - div10 - 1); d++) {
            if (n == d) continue;

            if (n * (d % 10) == div10 * d) {
                num *= n;
                den *= d;

                // std::cout << n << "/" << d << "\n";
            }
        }
    }

    qst::reduce(num, den);
    std::cout << den << std::endl;
}