/**
 * Finds the number of words in a file that are coded triangle numbers by 
 *  checking if the sum of the alphabetical positions of the letters in a word 
 *  is a triangle number
 * Problem description at https://projecteuler.net/problem=42
 * 
 * Usage: PROGRAM [FILE]
 * - FILE: The file containing the words to check
 * 
 * Restrictions:
 * - Assumes words in file are in quotes
 * 
 * Expected Output: 162
 */

#include <iostream>
#include <fstream>
#include <string>
#include <unordered_set>

int main(int argc, char** argv) {
    std::string file = (argc > 1) ? argv[1] : "../input/p042_words.txt";
    std::ifstream in(file);

    int maxT = 1;
    std::unordered_set<int> tNums = {maxT};
    
    int tWords = 0;
    std::string word = "";
    while(std::getline(in, word, ',')) {
        word = word.substr(1, word.length() - 2);

        // Finds the value of the word by adding its characters' values
        int val = 0;
        for (char c : word) 
            val += c - 'A' + 1;

        // Records triangle numbers
        while (val > maxT) {
            maxT += tNums.size() + 1;
            tNums.emplace(maxT);
        }

        // Checks if word is a coded triangle number
        if (tNums.count(val)) tWords++;
    }

    in.close();

    std::cout << tWords << std::endl;
}