/**
 * Finds the number of distinct terms in the sorted sequence made by taking a^b 
 *  with 2 <= a, b <= n
 * Problem description at https://projecteuler.net/problem=29
 * 
 * Usage: PROGRAM [NUMBER]
 * - NUMBER: The maximum value of a and b
 * 
 * Notes:
 * - If n = a^b, n < max, and b > 2, then for every power a^m, with 1 <= m < b, 
 *      exclude every m powers of n up to n^(m/b * max) (they are not distinct)
 *      - Ex. 32 = 2^5
 *        Every power of 32 up to 32^20 can be excluded as powers of 2 (2^1)
 *        Every two powers up to 32^40 can be excluded as powers of 4 (2^2)
 *        Every three powers up to 32^60 can be excluded as powers of 8 (2^3)
 *        Every four powers up to 32^80 can be excluded as powers of 16 (2^4)
 * - Has MPIR solution
 * 
 * Expected Output:
 * - I: 5, O: 15
 * - I: 100, O: 9183
 */

#include <iostream>
#include <vector>
#include <unordered_set>
#include <string>
#include <numeric>

int calcExclusions(int, int);

int main(int argc, char** argv) {
    int min = 2, max = 100;
    if (argc > 1) max = std::stoul(argv[1]);

    unsigned int cnt = 0;
    std::unordered_set<int> powers;

    int rt = sqrt(max);
    for (int a = min; a <= rt; a++) {
        if (powers.count(a)) continue;
        cnt += max - 1;

        // Calculates number of distinct powers contributed by every power of a less than max
        int b = 2;
        unsigned int n = a * a;
        while (n <= max) {
            powers.emplace(n);
            cnt += max - calcExclusions(b, max);
            n = pow(a, ++b);
        }
    }

    for (int a = rt + 1; a <= max; a++)
        if (powers.count(a) == 0) cnt += max - 1;

    std::cout << cnt << std::endl;
}

// Calculates number of powers already included as a power of a root
int calcExclusions(int b, int max) {
    int m = 2;
    int start = max / b;
    std::vector<bool> excludes;

    // If n = a^b and b is even, then n is a square
    if ((b & 1) == 0) {
        m = (b >> 1) + 1;
        start = max >> 1;
        excludes = std::vector<bool>(start - max / b, 0);
    } else excludes = std::vector<bool>(max - 2 * start, 0);
    start++;

    for (; m < b; m++) {
        int tmp = m / std::gcd(m, b);
        for (int i = start; i <= max * m / b; i++)
            if (i % tmp == 0) excludes[i - start] = 1;
    }

    int numEx = (start - 1);
    for (bool isEx : excludes)
        numEx += isEx;

    return numEx;
}