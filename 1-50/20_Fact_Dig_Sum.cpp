/**
 * Finds the sum of the digits of n!
 * Problem description at https://projecteuler.net/problem=20
 * 
 * Usage: PROGRAM [NUMBER]
 * - NUMBER: The factorial number
 * 
 * Notes:
 * - Has MPIR solution
 * 
 * Expected Output:
 * - I: 10, O: 27
 * - I: 100, O: 648
 */

#include <iostream>
#include <vector>
#include <string>
#include <math.h>

int main(int argc, char** argv) {
    int n = (argc > 1) ? std::stoul(argv[1]) : 100;

    std::vector<char> fact = {1};
    for (int i = 2; i <= n; i++) {
        int nDigs = floor(log10(i)) + 1;

        int carry = 0;
        for (size_t j = 0; j < fact.size(); j++) {
            carry += fact[j] * i;
            fact[j] = carry % 10;
            carry /= 10;
        }

        while (carry > 0) {
            fact.push_back(carry % 10);
            carry /= 10;
        }
    }

    // for (int i = fact.size() - 1; i > -1; i--)
    //     std::cout << (unsigned int) fact[i];

    int sum = 0;
    for (size_t i = 0; i < fact.size(); i++)
        sum += fact[i];
    std::cout << sum << std::endl;
}