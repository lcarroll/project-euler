/**
 * Finds the nth lexicographic permutation of the digits 0-9
 * Problem description at https://projecteuler.net/problem=24
 * 
 * Usage: PROGRAM [NUMBER]
 * - Number: The desired lexicographic permutation
 * 
 * Notes:
 * - Ex. When looking for the millionth permutation,
 *          2 * 9! (725760) permutations start with 0 or 1, and 3 * 9! (1088640)
 *          permutations start with 0, 1, or 2, thus the millionth permutation 
 *          must start with 2
 *       Next need to look for the 1000000 - 725760 = 274240th permutation of 
 *          the remaining numbers, and so on
 * 
 * Expected Output:
 * - I: 1000000, O: 2783915460
 */

#include <iostream>
// #include <algorithm>
#include <string>

int main(int argc, char** argv) {
    int n = (argc > 1) ? std::stoul(argv[1]) - 1 : 1000000 - 1;

    std::string digits = "0123456789";

    // for (int pNum = 0; pNum < n; pNum++)
    //     std::next_permutation(digits.begin(), digits.end());

    // std::cout << digits << std::endl;
    
    int fact = 1;
    for (int i = 2; i < digits.length(); i++)
        fact *= i;
    char fBase = digits.length() - 1;

    std::string perm = "";
    while (fBase > 0) {
        int i = n / fact;
        
        perm += digits[i];
        digits.erase(digits.begin() + i);

        n -= fact * i;
        fact /= fBase--;
    }
    perm += digits[0];

    std::cout << perm << std::endl;
}
