/**
 * Finds the next greatest triangle number (after T(285) = 40755) that is also
 *  pentagonal and hexagonal
 * Problem description at https://projecteuler.net/problem=45
 *
 * Usage: PROGRAM
 *
 * Notes:
 * - All hexagonal numbers are also triangular
 *
 * Expected Output: 1533776805
 */

#include <iostream>
#include <math.h>

unsigned int getHexNum(int);
bool isPentNum(unsigned int);

int main() {
    int n = 144;    // Given H(143) = T(285)

    unsigned int hex = getHexNum(n);
    while (!isPentNum(hex))
        hex = getHexNum(++n);

    std::cout << hex << std::endl;
}

unsigned int getHexNum(int n) {
    return n * (2 * n - 1);
}

bool isPentNum(unsigned int p) {
    double n = (sqrt(24 * p + 1) + 1) / 6;
        
    if (floor(n) == ceil(n)) return true;        
    return false;
}