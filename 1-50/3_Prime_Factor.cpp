/**
 * Finds the largest prime factor of a number
 * Problem description at https://projecteuler.net/problem=3
 * 
 * Usage: PROGRAM [NUMBER]
 * - NUMBER: The number whose largest prime factor should be calculated
 * 
 * Notes:
 * - Could also be implemented using a sieve
 * 
 * Expected Output:
 * - I: 13195, O: 29
 * - I: 600851475143, O: 6857
 */

#include <iostream>
#include <string>

int main(int argc, char** argv) {
    long long num = (argc > 1) ? std::stoll(argv[1]) : 600851475143;

    if (-2 < num && num < 2) std::cout << "N/A\n";

    while ((num & 1) == 0) num >>= 1;
    if (num == 1) {
        std::cout << 2;
        return 0;
    }
    
    long long divisor = 3;
    while (num > 1) {
        if (num % divisor == 0) {
            num /= divisor;
            std::cout << divisor << " ";
        } else divisor += 2;
    }

    // std::cout << divisor << std::endl;
}