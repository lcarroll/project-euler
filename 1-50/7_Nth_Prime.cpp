/**
 * Finds the nth prime number
 * Problem description at https://projecteuler.net/problem=7
 * 
 * Usage: PROGRAM [NUMBER]
 * - NUMBER: The target n
 * 
 * Expected Output:
 * - I: 6, O: 13
 * - I: 10001, O: 104743
 */

#include <iostream>
#include <vector>
#include <string>
#include <math.h>

int main(int argc, char** argv) {
    int sz = (argc > 1) ? std::stoul(argv[1]) : 10001;
    std::vector<int> primes = {2, 3};

    int n = 5;
    while (primes.size() < sz) {
        int rt = sqrt(n);

        bool isDiv = false;
        for (int i = 1; i < primes.size() && primes[i] <= rt; i++) {
            if (n % primes[i] == 0) {
                isDiv = true;
                break;
            }
        }

        if (!isDiv) primes.push_back(n);
        n += 2;
    }

    std::cout << primes[sz - 1] << std::endl;
}