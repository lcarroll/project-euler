/**
 * Finds the greatest product of n adjacent numbers in a grid
 * Problem description at https://projecteuler.net/problem=11
 * 
 * Usage: PROGRAM [-f FILE] [-n NUMBER]
 * - FILE: The input file containing the grid
 * - NUMBER: The number of adjacent integers
 * 
 * Notes:
 * - Starting from the top left, checking four directions is sufficient
 *      (down, right, down and to the left, down and to the right)
 * 
 * Expected Output: 70600674
 */

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include "../helper.hpp"

void initGrid(const std::string&, std::vector<std::vector<int>>&);

int main(int argc, char** argv) {
    std::string file = "../input/p011_grid.txt";
    int numAdj = 4;
    qst::checkInputs(argc, argv, file, numAdj);

    std::vector<std::vector<int>> grid;
    initGrid(file, grid);
    
    int numRows = grid.size();
    int rowLen = (!grid.empty()) ? grid[0].size() : 0;

    if (numAdj > numRows && numRows >= rowLen) numAdj = numRows;
    else if (numAdj > rowLen && rowLen > numRows) numAdj = rowLen;

    // Calculates four products (or as many as possible) for each number in grid
    long long max = 0;
    for (int i = 0; i < numRows; i++) {
        for (int j = 0; j < rowLen; j++) {
            int p = 1;
            if (i <= numRows - numAdj) {    // down
                for (int k = 0; k < numAdj; k++)
                    p *= grid[i + k][j];
                if (p > max) max = p;

                if (j >= numAdj - 1) {      // down left
                    p = 1;
                    for (int k = 0; k < numAdj; k++)
                        p *= grid[i + k][j - k];
                    if (p > max) max = p;    
                }

                if (j <= rowLen - numAdj) { // down right
                    p = 1;
                    for (int k = 0; k < numAdj; k++)
                        p *= grid[i + k][j + k];
                    if (p > max) max = p;
                }
            }

            if (j <= rowLen - numAdj) {     // right
                p = 1;
                for (int k = 0; k < numAdj; k++)
                    p *= grid[i][j + k];
                if (p > max) max = p;
            }
        }
    }

    std::cout << max << std::endl;
}

// Reads grid from file
void initGrid(const std::string& file, std::vector<std::vector<int>>& grid) {
    std::ifstream in(file);

    std::string line;
    while (getline(in, line) && !line.empty()) {
        std::istringstream iss(line);
        
        std::vector<int> row;
        std::string num;
        while (iss >> num)
            row.push_back(stoi(num));

        grid.push_back(row);
    }
    
    in.close();
}