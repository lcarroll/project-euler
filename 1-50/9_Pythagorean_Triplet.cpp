/**
 * Calculates the product of the Pythagorean triplet that adds up to a number
 * Problem description at https://projecteuler.net/problem=9
 * 
 * Usage: PROGRAM [NUMBER]
 * - NUMBER: The target sum of a hypothetical Pythagorean triplet
 * 
 * Notes:
 * - c must be greater than a and b, thus a and b must each be less than half 
 *      the target number
 * - a and b cannot be equal
 * 
 * Expected Output:
 * - I: 12 (3 + 4 + 5), O: 60
 * - I: 1000, O: 31875000
 */

#include <iostream>
#include <string>

int main(int argc, char** argv) {
    int goal = (argc > 1) ? std::stoul(argv[1]) : 1000;

    for (int a = 1; a < goal >> 1; a++) {
        for (int b = a + 1; b < goal >> 1; b++) {
            int c = goal - a - b;
            if (a * a + b * b == c * c) {
                std::cout << a << " * " << b << " * " << c << " = ";
                std::cout << a * b * c << std::endl;
                return 0;
            }
        }
    }

    std::cout << "No triplet (a, b, c) found such that a + b + c = "; 
    std::cout << goal << std::endl;
}