/**
 * Finds the sum of all integers which cannot be written as the sum of two 
 *  abundant numbers
 * Problem description at https://projecteuler.net/problem=23
 *
 * Usage: PROGRAM
 * 
 * Definitions:
 * - Abundant numbers are numbers whose proper divisors sum to more than the 
 *      original number
 * 
 * Expected Output: 4179871
 */

#include <iostream>
#include <vector>
#include "../helper.hpp"

typedef unsigned short ushort;

int main() {
    int upperLim = 28123;   // Given
    
    std::vector<ushort> abundants;
    std::vector<bool> isSum(upperLim, false);
    for (ushort i = 12; i <= upperLim - 12; i++) {
        if (qst::d(i) > i) {
            abundants.push_back(i);
            for (ushort num : abundants) {
                if (i + num <= upperLim) isSum[i + num - 1] = true;
                else break;
            }
        }
    }
    
    int sum = 0;
    for (ushort i = 0; i < isSum.size(); i++)
        if (!isSum[i]) sum += i + 1;

    std::cout << sum << std::endl;
}