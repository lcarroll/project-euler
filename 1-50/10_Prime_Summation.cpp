/**
 * Finds the sum of all primes below a number
 * Problem description at https://projecteuler.net/problem=10
 * 
 * Usage: PROGRAM [NUMBER]
 * - NUMBER: The number up to which to check for primes
 * 
 * Expected Output:
 * - I: 10, O: 17
 * - I: 2000000, O: 142913828922
 */

#include <iostream>
#include "../helper.hpp"

int main(int argc, char** argv) {
    int maxNum = (argc > 1) ? std::stoi(argv[1]) : 2000000;
    if (maxNum < 3) {
        std::cout << 0 << std::endl;
        return 0;
    }

    // Uses a sieve to find primes
    std::vector<bool> isPrime = qst::findPrimes(maxNum);

    long long sum = 2;
    for (int i = 0; i < isPrime.size(); i++)
        sum += (isPrime[i]) ? (i << 1) + 3 : 0;
    
    std::cout << sum << std::endl;
}