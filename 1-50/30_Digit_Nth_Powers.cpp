/**
 * Finds the sum of all the numbers that can be written as the sum of the nth
 *  powers of all their digits
 * Problem description at https://projecteuler.net/problem=30
 * 
 * Restrictions:
 * - 1 <= n <= 8
 * 
 * Notes:
 * - The highest number that could possibly be the sum of the nth powers of its 
 *      digits is 9^n * (n + 1)
 * - Can check each unique set of digits rather than every number to reduce
 *      number of checks
 * 
 * Expected Output:
 * - I: 3, O: 1301
 * - I: 4, O: 19316
 * - I: 5, O: 443839
 * - I: 6, O: 548834
 * - I: 7, O: 40139604
 * 
 * Change Log:
 * - 2022-09-02 Change to check unique sets of digits
 */

#include <iostream>
#include <unordered_set>
#include <vector>
#include <string>
#include <math.h>

int main(int argc, char** argv) {
    int exp = (argc > 1) ? std::stoul(argv[1]) : 5;

    int powers[10];
    for (int i = 0; i < 10; i++)
        powers[i] = pow(i, exp);

    std::unordered_set<int> sumSet;

    int nDigs = log10(powers[9] * (exp + 1)) + 1;
    std::vector<char> num(nDigs, 0);
    num[nDigs - 1] = 2;     // Excludes 1 from the final sum

    int sum = 0;
    while (true) {
        int digPowSum = 0;
        for (int i = 0; i < nDigs; i++)
            digPowSum += powers[num[i]];

        int tmp = digPowSum, sumCheck = 0;
        while (tmp > 0) {
            sumCheck += powers[tmp % 10];
            tmp /= 10;
        }

        if (digPowSum == sumCheck && sumSet.count(digPowSum) == 0) {
            sum += digPowSum;
            sumSet.emplace(digPowSum);

            std::cout << digPowSum << " + ";
        }

        // Generates next unique set of digits
        int index = nDigs - 1;
        for (; index >= 0 && num[index] == 9; index--);

        if (index < 0) break;
        else num[index]++;

        for (; index < nDigs - 1; index++)
            num[index + 1] = num[index];
    }

    // int max = powers[9] * (exp + 1);

    // int sum = 0;
    // for (int i = 1 << exp; i <= max; i++) {
    //     int n = i;
    //     int digSum = 0;
    //     while (n > 0) {
    //         digSum += powers[n % 10];
    //         n /= 10;
    //     }

    //     if (digSum == i) {
    //         sum += i;
    //         std::cout << i << " + ";
    //     }
    // }

    std::cout << "\b\b= " << sum << "\n";
}
